const mongoose = require("mongoose");
const validator = require("validator");

const userSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      required: [true, "Please Enter Your Username"],
      maxLength: [30, "Username cannot excedd 30 characters"],
      minLength: [2, "Username should have more than 2 characters"],
      trim: true,
    },
    email: {
      type: String,
      required: [true, "Please Enter Your email"],
      unique: true,
      validate: [validator.isEmail, "Please Enter Valid Email"],
      trim: true,
    },
    password: {
      type: String,
      required: [true, "Please Enter Your Password"],
      validate: [
        validator.isStrongPassword,
        "Password must contains minimum 8 char,1 lowercase,1 uppercase,1 numbers,1 symbols",
      ],
      trim: true,
    },
    profileImage: {
      type: String,
    },
    role: {
      type: String,
      default: "User",
    },
    resetPasswordToken: String,
    resetPasswordExpire: Date,
  },
  { timestamps: true },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.password;
      },
    },
  }
);

const User = mongoose.model("user", userSchema);

module.exports = User;
