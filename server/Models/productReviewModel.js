const mongoose = require("mongoose");

const productReviewSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, "User id is required"],
      ref: "User",
    },
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, "Product id is required"],
      ref: "Product",
    },
    title: {
      type: String,
      required: [true, "Title is required"],
    },
    reviewText: {
      type: String,
    },
    rating: {
      type: Number,
      required: [true, "Rating is required"],
    },
  },
  { timestamps: true }
);

const Product_Review = mongoose.model("product_review", productReviewSchema);

module.exports = Product_Review;
