const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      // required: [true, "userId is required"],
      ref: "User",
    },
    title: {
      type: String,
      required: [true, "Product title is required"],
    },
    description: {
      type: String,
      required: [true, "Product description is required"],
    },
    price: {
      type: Number,
      required: [true, "Product price is required"],
    },
    discountPercentage: {
      type: Number,
    },
    rating: {
      type: Number,
    },
    stock: {
      type: Number,
      // required: [true, "Product stock is required"],
    },
    brand: {
      type: String,
      // required: [true, "Product brand is required"],
    },
    category: {
      type: String,
      // required: [true, "Product category is required"],
    },
    thumbnail: {
      type: Object,
      // required: [true, "Product thumbnail is required"],
    },
    images: Array,
  },
  { timestamps: true }
);

const Product = mongoose.model("product", productSchema);

module.exports = Product;
