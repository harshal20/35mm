const mongoose = require("mongoose");

// Connect MongoDB
const dbConnect = (MONGO_URL) => {
  mongoose
    .connect(MONGO_URL, {
      useNewUrlParser: true,
    })
    .then(() => {
      console.log("DB connected successfully.");
    })
    .catch((error) => {
      console.log("DB connection failed \n" + error);
    });
};

module.exports = dbConnect;
