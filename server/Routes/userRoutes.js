const express = require("express");
const {
  Register,
  login,
  getAllUser,
  getSingleUser,
  updateUser,
  deleteUser,
} = require("../Controllers/userController");
const verifyToken = require("../Middleware/jwtVerifyToken");
const router = express.Router();

router.post("/register", Register);
router.post("/login", login);
router.get("/allUsers", verifyToken, getAllUser);
router.get("/:id", verifyToken, getSingleUser);
router.put("/:id", verifyToken, updateUser);
router.delete("/:id", verifyToken, deleteUser);

module.exports = router;  

