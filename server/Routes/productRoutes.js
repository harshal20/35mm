const express = require("express");
const { verify } = require("jsonwebtoken");
const {
  addProduct,
  getAllProduct,
  getSingleProduct,
  deleteProduct,
  updateProduct,
} = require("../Controllers/productController");
const verifyToken = require("../Middleware/jwtVerifyToken");
const upload = require("../Middleware/multerUpload");
const router = express.Router();

router.post(
  "/add",
  upload.fields([
    {
      name: "product_files",
    },
    {
      name: "thumbnail",
      maxCount: 1,
    },
  ]),
  addProduct
);

router.get("/allProducts", getAllProduct);
router.get("/:id", verifyToken, getSingleProduct);
router.put("/:id", verifyToken, updateProduct);
router.delete("/:id", verifyToken, deleteProduct);

module.exports = router;
