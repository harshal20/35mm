const express = require("express");
const verifyToken = require("../Middleware/jwtVerifyToken");
const {
  addProductReview,
  getAllProductsReview,
  singleProductsReview,
  updateProductReview,
  deleteProductReview,
} = require("../Controllers/productReviewController");
const router = express.Router();

router.post("/add", verifyToken, addProductReview);
router.post("/allReview", verifyToken, getAllProductsReview);
router.get("/singleReview/:id", verifyToken, singleProductsReview);
router.put("/:id", verifyToken, updateProductReview);
router.delete("/:id", verifyToken, deleteProductReview);

module.exports = router;
