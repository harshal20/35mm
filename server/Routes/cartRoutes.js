const express = require("express");
const { addCart } = require("../Controllers/cartController");
const verifyToken = require("../Middleware/jwtVerifyToken");
const router = express.Router();

router.post("/add", verifyToken, addCart);

module.exports = router;
