const Product = require("../Models/productModel");
const Product_Review = require("../Models/productReviewModel");
const User = require("../Models/userModel");

exports.addProductReview = async (req, res, next) => {
  try {
    const userId = await User.findById(req.body.userId);
    const productId = await Product.findById(req.body.productId);
    if (userId) {
      if (productId) {
        const productReview = await Product_Review.create(req.body);
        res.status(201).send({ success: true, data: productReview });
      } else {
        res.status(404).send({
          success: false,
          message: "ProductId is not found in our database",
        });
      }
    } else {
      res.status(404).send({
        success: false,
        message: "userId is not found in our database",
      });
    }
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.getAllProductsReview = async (req, res, next) => {
  try {
    if (!req.body.productId) {
      res
        .status(400)
        .send({ success: false, message: "productId is required" });
    } else {
      const _id = await req.body.productId;
      const productId = await Product.findById(_id);
      if (productId) {
        const allProductReview = await Product_Review.find({
          productId: req.body.productId,
        });
        res.status(200).send({ success: true, data: allProductReview });
      } else {
        res.status(404).send({
          success: false,
          messahge: "productId is not found in our database",
        });
      }
    }
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.singleProductsReview = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const productReview = await Product_Review.findById(_id);
    if (productReview) {
      res.status(200).send({ success: true, data: productReview });
    } else {
      res.status(404).send({
        success: false,
        message:
          "productReview is not found in our database,please check your id",
      });
    }
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.updateProductReview = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const deleteReview = await Product_Review.findByIdAndUpdate(_id, req.body, {
      new: true,
    });
    res.status(200).send({ success: true, data: deleteReview });
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.deleteProductReview = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const deleteReview = await Product_Review.findByIdAndDelete(_id);
    res.status(200).send({ success: true, data: deleteReview });
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};
