const Product = require("../Models/productModel");

exports.addProduct = async (req, res, next) => {
  try {
    const {
      // userID,
      title,
      description,
      price,
      discountPercentage,
      rating,
      stock,
      brand,
      category,
    } = await JSON.parse(req.body.data);

    if (req.files.product_files && req.files.thumbnail) {
      const productData = await Product.create({
        // userID,
        title,
        description,
        price,
        discountPercentage,
        rating,
        stock,
        brand,
        category,
        thumbnail: {
          name: req.files.thumbnail[0].filename,
          path: req.files.thumbnail[0].path.replaceAll("\\", "/"),
        },
        images: req.files.product_files.map((item, index) => {
          return {
            name: item.filename,
            path: item.path.replaceAll("\\", "/"),
          };
        }),
      });
      res.status(201).send({ succes: true, data: productData });
    } else if (!req.files.product_files) {
      res.status(400).send({
        succes: false,
        message: "images are required, please upload images..",
      });
    } else {
      res.status(400).send({
        success: false,
        message: "thumbnail image is required, please upload thumbnail image..",
      });
    }
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.getAllProduct = async (req, res, next) => {
  try {
    const allProducts = await Product.find();
    res.status(200).send({ succes: true, data: allProducts });
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.getSingleProduct = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const allProducts = await Product.findById(_id);
    res.status(200).send({ succes: true, data: allProducts });
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.updateProduct = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const updateProduct = await Product.findByIdAndUpdate(_id, req.body);
    res.status(200).send({ succes: true, data: updateProduct });
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};

exports.deleteProduct = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const deleteProduct = await Product.findByIdAndDelete(_id, req.body);
    res.status(200).send({ succes: true, data: deleteProduct });
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};
