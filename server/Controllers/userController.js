const User = require("../Models/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
// Create user
exports.Register = async (req, res, next) => {
  try {
    const { userName, email, password } = req.body;
    const bcryptPassword = await bcrypt.hash(password, 12);
    const data = await User.create({
      userName,
      email,
      password: bcryptPassword,
    });
    res.status(201).send({ success: true, data });
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};
// Find user and login
exports.login = async (req, res, next) => {
  try {
    const { email, password } = await req.body;
    const findUser = await User.findOne({
      email: email,
    });
    if (findUser) {
      const isPasswordMatch = await bcrypt.compare(password, findUser.password);
      if (isPasswordMatch) {
        const token = await jwt.sign(
          { _id: findUser._id, email: findUser.email },
          process.env.JWTSECRETKEY,
          {
            expiresIn: 2 * 24 * 60 * 60, //Expire after 2 Days
          }
        );
        res.status(200).send({ success: true, data: findUser, token });
      } else {
        res
          .status(401)
          .send({ success: false, message: "Email or password is invalid" });
      }
    } else {
      res
        .status(401)
        .send({ success: false, message: "Email or password is invalid" });
    }
  } catch (error) {
    res.status(400).send(error);
    console.log(error);
  }
};
exports.getAllUser = async (req, res, next) => {
  try {
    const allUser = await User.find();
    res.status(200).send({ success: true, data: allUser });
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.getSingleUser = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const user = await User.findById(_id);
    res.status(200).send({ success: true, data: user });
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.updateUser = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const user = await User.findByIdAndUpdate(_id, req.body, { new: true });
    res.status(200).send({ success: true, data: user });
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.deleteUser = async (req, res, next) => {
  try {
    const _id = await req.params.id;
    const user = await User.findByIdAndDelete(_id);
    res.status(200).send({ success: true, data: user });
  } catch (error) {
    res.status(400).send(error);
  }
};
