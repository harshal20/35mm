const multer = require("multer");
const path = require("path");


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "media/images");
  },
  filename: function (req, file, cb) {
    const filename = file.originalname
      .toLowerCase()
      .replace(" ", "-")
      .split(".");
    cb(null, filename[0] + "_" + Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg"
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error("Only .png, .jpg and .jpeg images allowed!"));
    }
  },
});

module.exports = upload;
