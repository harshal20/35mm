const jwt = require("jsonwebtoken");

const verifyToken = async (req, res, next) => {
  try {
    const token = await req.headers.authorization;
    const verifyToken = await jwt.verify(token, process.env.JWTSECRETKEY);
    next();
  } catch (error) {
    res.status(401).send(error);
  }
};

module.exports = verifyToken;
