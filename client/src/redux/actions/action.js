//  add
export const ADD = (item) => {
  return {
    type: "ADD_CART",
    payload: item,
  };
};

// remove Cart
export const DELETE = (id) => {
  return {
    type: "REMOVE_CART",
    payload: id,
  };
};

// delete Item
export const REMOVE_ITEM = (item) => {
  return {
    type: "REMOVE_ITEM",
    payload: item,
  };
};
