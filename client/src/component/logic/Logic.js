import { useEffect } from "react";
import Cookies from "js-cookie";
import { useLocation } from "react-router-dom";

const Logic = () => {
  useEffect(() => {
    // cookies remove
    const timeOut = setTimeout(() => {
      Cookies.remove("token");
      window.location.reload();
    }, 24 * 60 * 60 * 1000);

    return () => clearTimeout(timeOut);
  }, []);

  return <></>;
};

const RouteToTop = (props) => {
  const location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  return <>{props.children}</>;
};

export { Logic, RouteToTop };
