import React, { useEffect, useState } from "react";
import "../HeaderFooterCSS/headerfooter.css";
import { Link } from "react-router-dom";
import { headerlogo } from "../../assets/images";
import Cookies from "js-cookie";
import { Badge } from "@mui/material";
import Menu from "@mui/material/Menu";
import { useDispatch, useSelector } from "react-redux";
import { DELETE } from "../../redux/actions";
import Visit from "../Visit/Visit";
import { Links, ViewCart } from "../layout/Buttons";
const Header = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [price, setPrice] = useState(0);
  const [heart, setHeart] = useState();

  const dispatch = useDispatch();
  // redux useSelector
  const getData = useSelector((state) => state.cartreducer.carts);
  // console.log(getData);
  const getCookies = Cookies.get("token");
  // logout
  const logOut = () => {
    Cookies.remove("token");
    window.location.reload();
  };
  // mui Menu
  const open = Boolean(anchorEl);
  const openvisitlist = Boolean(heart);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClick1 = (event) => {
    setHeart(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClose1 = () => {
    setHeart(null);
  };

  // remove item
  const deleteCart = (id) => {
    dispatch(DELETE(id));
  };
  // total price
  const total = () => {
    let price = 0;
    getData.map((element, key) => {
      price = element.price * element.quantity + price;
    });
    setPrice(price);
  };
  useEffect(() => {
    total();
  }, [total]);

  return (
    <>
      <header>
        <nav>
          <div className="container">
            <div className="searchbar">
              <Link to="/">
                <img src={headerlogo} alt="headerlogo" />
              </Link>
              <div className="logo">
                <div className="homedetail">
                  <ul className="home">
                    <li>
                      <Link to="/">HOME</Link>
                    </li>
                    <li>
                      <Link to="/">MEN</Link>
                    </li>
                    <li>
                      <Link to="/">WOMEN</Link>
                    </li>
                    <li>
                      <Link to="/">KIDS</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="cart">
                <ul className="icons">
                  <li>
                    <Link to={"/"}>

                      {/* <form class="search-form">
                        <span><i class="bx bx-arrow-back search-cancel"></i></span>
                        <input type="search" name="search" class="search-input" placeholder="Search here..." />
                      </form> */}
                      <div className="searchInput_Container">
                        <input id="searchInput" type="text" placeholder="Search here..." onChange={(event) => {
                          setSearchTerm(event.target.value);
                        }} />
                      </div>


                      {/* <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="20.314"
                        height="20.314"
                        viewBox="0 0 20.314 20.314"
                      >
                        <path
                          id="Path_2"
                          data-name="Path 2"
                          d="M11,2a9,9,0,1,1-9,9A9,9,0,0,1,11,2Zm0,16a7,7,0,1,0-7-7A7,7,0,0,0,11,18Zm8.485.071L22.314,20.9,20.9,22.314l-2.828-2.829,1.414-1.414Z"
                          transform="translate(-2 -2)"
                          fill="#8392A5"
                        />
                      </svg> */}
                    </Link>
                  </li>

                  <li>
                    <Link to={"/"}>
                      <svg
                        id="sound-module-line"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                      >
                        <path
                          id="Path_3"
                          data-name="Path 3"
                          d="M0,0H24V24H0Z"
                          fill="none"
                        />
                        <path
                          id="Path_4"
                          data-name="Path 4"
                          d="M21,18v3H19V18H17V16h6v2ZM5,18v3H3V18H1V16H7v2ZM11,6V3h2V6h2V8H9V6Zm0,4h2V21H11ZM3,14V3H5V14Zm16,0V3h2V14Z"
                          fill="#1C2D41"
                        />
                      </svg>
                    </Link></li >
                  <li>
                    <Link to={"/"}>
                      <svg
                        id="heart-3-line_5_"
                        data-name="heart-3-line (5)"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                      >
                        <path
                          id="Path_5"
                          data-name="Path 5"
                          d="M0,0H24V24H0Z"
                          fill="none"
                        />
                        <path
                          id="Path_6"
                          data-name="Path 6"
                          d="M16.5,3C19.538,3,22,5.5,22,9c0,7-7.5,11-10,12.5C9.5,20,2,16,2,9A5.675,5.675,0,0,1,7.5,3,6.617,6.617,0,0,1,12,5,6.617,6.617,0,0,1,16.5,3ZM12.934,18.6a26.953,26.953,0,0,0,2.42-1.7C18.335,14.533,20,11.943,20,9c0-2.36-1.537-4-3.5-4a4.608,4.608,0,0,0-3.086,1.414L12,7.828,10.586,6.414A4.608,4.608,0,0,0,7.5,5C5.56,5,4,6.656,4,9c0,2.944,1.666,5.533,4.645,7.9a26.908,26.908,0,0,0,2.421,1.7c.3.189.6.37.934.572.339-.2.635-.383.934-.571Z"
                          fill="#1c2d41"
                        />
                      </svg>
                    </Link>
                  </li>

                  <li>
                    <Badge
                      badgeContent={getData.length}
                      color="primary"
                      id="basic-button"
                      aria-controls={open ? "basic-menu" : undefined}
                      aria-haspopup="true"
                      aria-expanded={open ? "true" : undefined}
                      onClick={handleClick}
                    >
                      <FiShoppingBag className="shopping_icon" />
                    </Badge>
                    {/* </button> */}
                  </li>
                  <li>
                    {!getCookies ? (
                      <Link to={"/login"}>Sign In</Link>
                    ) : (
                      <Link onClick={logOut}>Sign Out</Link>
                    )}
                  </li>
                </ul >
              </div >
            </div >
          </div >
        </nav >
      </header >
      <div className="visit_visit">
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          {getData.length ? (
            <div>
              {getData.map((e) => {
                return (
                  <>
                    <div className="visit ">
                      <div className="viditlist cartvisit">
                        <div className="imageflex">
                          <Link
                            to={`/Produtviewpage/${e.id}`}
                            onClick={handleClose}
                          >
                            <img
                              src={e.image}
                              style={{ width: "5rem", height: "5rem" }}
                              alt=""
                            />
                          </Link>
                        </div>
                        {/* <br /> */}
                        {/* <h4>{e.title}</h4>
                      <p>Price : ${e.price}</p>
                       <p>Quantity : {e.quantity}</p> */}
                        <div className="dreessflex">
                          <h4> Long Strappy Dress</h4>
                          <span>{e.quantity}</span>
                          <span>x</span>
                          <span style={{ color: "#1190CB" }}>{e.price}</span>
                        </div>
                        <button
                          className="deletebtn"
                          onClick={() => deleteCart(e.id)}
                        >
                          <i className="fas fa-trash largetrash "></i>
                        </button>
                      </div>

                    </div>
                  </>

                );
              })}
              {/* <div className="cart_total">{total}</div> */}
              <div className="totalsub">
                <h4>Subtotal</h4>
                <h3>{price}</h3>
              </div>
              <div className="viewcart ">
                <ViewCart link={"/"} viewsub={"View Cart"} />
                <Links link={"/"} buttons={"Checkout"}></Links>
              </div>
            </div>
          ) : (
            <div className="emptybox" style={{ position: "relative" }}>
              <i
                className="fas fa-close smallclose"
                onClick={handleClose}
                style={{
                  position: "absolute",
                  top: 2,
                  right: 20,
                  fontSize: 23,
                  cursor: "pointer",
                }}
              ></i>
              <p className="cartsempty" style={{ fontSize: 18 }}>
                Your carts is empty
              </p>
            </div>
          )}
        </Menu>
      </div>
      {/* visit list  */}
      <div className="heart_heart">
        <Menu
          id="basic-menu"
          anchorEl={heart}
          open={openvisitlist}
          onClose={handleClose1}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}>
          <Visit />
        </Menu>
      </div>

    </>
  );
};


export default Header;
