import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import endimg from "../../../assets/images/endimg.png";
import Rating from "@mui/material/Rating";
import "./Productview.css";
import color1 from "../../../assets/images/productview/color1.png";
import color2 from "../../../assets/images/productview/color2.png";
import color3 from "../../../assets/images/productview/color3.png";
import { useDispatch, useSelector } from "react-redux";
import { ADD, DELETE, REMOVE_ITEM } from "../../../redux/actions";
import { Swiper } from 'swiper';
import ProductImagesSlider from "../productslider/ProductImagesSlider";
import { productImages } from "../../../assets/slideimg";


const Productview = () => {


  const dispatch = useDispatch();
  const [data, setData] = useState([]);

  const { id } = useParams();
  // add quantity
  const addQuantity = (e) => {
    dispatch(ADD(e));
  };
  // redux
  const getData = useSelector((state) => state.cartreducer.carts);
  // dynamic data
  const compare = () => {
    let compareData = getData.filter((e) => {
      return e.id == id;
    });
    setData(compareData);
  };
  // remove quantity
  const removeQuantity = (item) => {
    dispatch(REMOVE_ITEM(item));
  };
  // delete
  // const delete = (id) => {
  //   dispatch(DELETE(id));
  // }
  useEffect(() => {
    compare();
  }, [id]);
  return (
    <>
      <section className="Productviews">
        <Container>
          <Row>
            <Col lg={6}>
              <div className="productslider">

                <ProductImagesSlider images={productImages} />
              </div>
              {/* {data.map((item, index) => {
                return (
                  <div className="imageslides" key={index}>
                    <img src={item.image} />
                  </div>
                );
              })} */}








            </Col>
            <Col lg={6}>
              <div className="dresslist">
                <div className="product_total_price_main">
                  <h4>
                    <span>Category</span>
                    <p>Men Clothes</p>{" "}
                  </h4>
                  <h2>Long Strappy Dress</h2>
                </div>
                <div className="stars">
                  <div className="startflex">
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                        fill="#FF7D51"
                      />
                    </svg>
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                        fill="#FF7D51"
                      />
                    </svg>
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                        fill="#FF7D51"
                      />
                    </svg>
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                        fill="#FF7D51"
                      />
                    </svg>
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                        fill="#C0CCDA"
                      />
                    </svg>
                    5.0 (280)
                    <div className="seller35">
                      <span>Seller</span>
                      <Link>35mm Enterprise</Link>
                    </div>
                  </div>
                  <div className="imgbetween">
                    <img src={endimg} alt="" />
                  </div>
                </div>
                <div className="dollar">
                  <div className="prisedress">
                    <h3>$300.00</h3>
                    <span>$400.00</span>
                  </div>
                  <div className="wishlist">
                    <Link>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M16.5 3C19.538 3 22 5.5 22 9C22 16 14.5 20 12 21.5C9.5 20 2 16 2 9C2 5.5 4.5 3 7.5 3C9.36 3 11 4 12 5C13 4 14.64 3 16.5 3ZM12.934 18.604C13.815 18.048 14.61 17.495 15.354 16.903C18.335 14.533 20 11.943 20 9C20 6.64 18.463 5 16.5 5C15.424 5 14.26 5.57 13.414 6.414L12 7.828L10.586 6.414C9.74 5.57 8.576 5 7.5 5C5.56 5 4 6.656 4 9C4 11.944 5.666 14.533 8.645 16.903C9.39 17.495 10.185 18.048 11.066 18.603C11.365 18.792 11.661 18.973 12 19.175C12.339 18.973 12.635 18.792 12.934 18.604Z"
                          fill="#1C2D41"
                        />
                      </svg>
                      Add to Wishlist
                    </Link>
                  </div>
                </div>
                <div className="color">
                  <h4>Color</h4>
                  <div className="colorchange">
                    <Link>
                      <span className="">
                        <img src={color1} alt="" />
                      </span>
                    </Link>
                    <Link>
                      <span className="">
                        <img src={color2} alt="" />
                      </span>
                    </Link>
                    <Link>
                      <span className="">
                        <img src={color3} alt="" />
                      </span>
                    </Link>
                  </div>
                  <div className="smallsize">
                    <h4>Size</h4>
                    <input type="text" placeholder="S" />
                    <input type="text" placeholder="M" />
                    <input type="text" placeholder="L" />
                  </div>

                  <div className="size">
                    <div className="qua">
                      <h4>Quantity</h4>
                      <button
                        // onClick={ele.qunt <= 1 ? () => delete(ele.id) : () => removeQuantity(ele)}
                        className="quantity"
                      >
                        -
                      </button>
                      <input type="text" placeholder="2" />

                      <button
                        // onClick={() => addQuantity(element)}
                        className="quantity"
                      >
                        +
                      </button>
                    </div>
                    <div className="add">
                      <svg
                        width="22"
                        height="18"
                        viewBox="0 0 22 18"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M20 15V18H18V15H16V13H22V15H20ZM4 15V18H2V15H0V13H6V15H4ZM10 3V0H12V3H14V5H8V3H10ZM10 7H12V18H10V7ZM2 11V0H4V11H2ZM18 11V0H20V11H18Z"
                          fill="#1C2D41"
                        />
                      </svg>
                      <span>Add to Compare</span>
                    </div>
                  </div>
                  <div className="addtocart">
                    <Link className="cartsss" to="">
                      Add to Cart
                    </Link>
                    <Link className="room">
                      <svg
                        width="23"
                        height="19"
                        viewBox="0 0 23 19"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M21.0127 13.1926L15.2157 9.70515C14.4013 9.21531 13.4956 8.90439 12.5584 8.78745V7.9139C12.5584 7.42176 12.8402 6.96365 13.2937 6.71815C14.4206 6.10825 15.0842 4.9336 15.0251 3.65247C14.9471 1.95559 13.5752 0.581566 11.8762 0.503554C9.97162 0.416706 8.41327 1.93302 8.41327 3.80769C8.41327 4.27023 8.78831 4.64509 9.25068 4.64509C9.71321 4.64509 10.0881 4.27022 10.0881 3.80769C10.0881 2.88066 10.8586 2.13322 11.7992 2.17656C12.6317 2.21483 13.3139 2.89702 13.3521 3.72951C13.3814 4.36312 13.0535 4.94391 12.4964 5.24534C11.5017 5.78376 10.8836 6.80631 10.8836 7.9139V8.78696C9.95036 8.90275 9.04786 9.21106 8.23582 9.69698L2.43062 13.171C1.54955 13.6984 1.00115 14.6638 1 15.6908C0.998203 17.3159 2.31171 18.6326 3.93696 18.6346L19.4935 18.6528H19.4971C21.1168 18.6528 22.4355 17.3358 22.4375 15.7156C22.4386 14.6888 21.8926 13.7219 21.0127 13.1926ZM19.4971 16.978C19.4966 16.978 19.4959 16.978 19.4956 16.978L3.93906 16.9598C3.241 16.959 2.67396 16.3906 2.67478 15.6927C2.67527 15.2507 2.91128 14.8351 3.29056 14.608L9.09576 11.1341C10.7136 10.1659 12.7349 10.1674 14.3523 11.1402L20.1495 14.6277C20.5281 14.8555 20.7631 15.2717 20.7626 15.7137C20.7618 16.4111 20.1943 16.978 19.4971 16.978Z"
                          fill="white"
                          stroke="white"
                          stroke-width="0.5"
                        />
                      </svg>
                      Try Room
                    </Link>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
export default Productview;
