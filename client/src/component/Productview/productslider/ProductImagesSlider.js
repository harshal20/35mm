
// import PropTypes from 'prop-types'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Thumbs } from 'swiper'
import { useState } from 'react'
import "./ProductImagesSlider.css"
// import slider01 from "../../../../src/assets/images/Product slider/slider01.jpg"
// import slider02 from "../../../../src/assets/images/Product slider/slider02.jpg"
// import slider03 from "../../../../src/assets/images/Product slider/slider03.jpg"
// import slider04 from "../../../../src/assets/images/Product slider/slider04.jpg"
// import slider05 from "../../../../src/assets/images/Product slider/slider05.jpg"
// import slider06 from "../../../../src/assets/images/Product slider/slider06.jpg"

const ProductImagesSlider = props => {
  const [activeThumb, setActiveThumb] = useState()

  return <>
    <Swiper
      loop={true}
      spaceBetween={10}
      navigation={true}
      modules={[Navigation, Thumbs]}
      grabCursor={true}
      thumbs={{ swiper: activeThumb }}
      className='product-images-slider'
    >
      {
        props.images.map((item, index) => (
          <SwiperSlide key={index}>
            <img src={item} alt="product images" />
          </SwiperSlide>
        ))
      }
    </Swiper>
    <Swiper
      onSwiper={setActiveThumb}
      loop={true}
      spaceBetween={10}
      slidesPerView={4}
      modules={[Navigation, Thumbs]}
      className='product-images-slider-thumbs'
    >
      {
        props.images.map((item, index) => (
          <SwiperSlide key={index}>
            <div className="product-images-slider-thumbs-wrapper">
              <img src={item} alt="product images" />
              {/* <img src={slider01} alt="product images" />
              <img src={slider02} alt="product images" />
              <img src={slider03} alt="product images" />
              <img src={slider04} alt="product images" />
              <img src={slider05} alt="product images" />
              <img src={slider06} alt="product images" /> */}
            </div>
          </SwiperSlide>
        ))
      }
    </Swiper>
  </>
}

// ProductImagesSlider.propTypes = {
//   images: PropTypes.array.isRequired
// }

export default ProductImagesSlider