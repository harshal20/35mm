import React from 'react'
import { Container, Row } from 'react-bootstrap'
import "./Description.css"
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
// import Title from '../../layout/Title';

const Description = () => {
    return (
        <>
            <section className='Description'>
                <Container>
                    <Row>
                        <div className="sellingproduct">
                            <div className="productnav descrp" >
                                <Tabs
                                // defaultActiveKey="profile"
                                // id="uncontrolled-tab-example"
                                // className="mb-3"
                                >
                                    <Tab eventKey="Description" title="Description">
                                        <ul className="desdetail">
                                            <li>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam ex, pellentesque vel vestibulum vitae, elementum consectetur libero. Ut vitae lobortis ex. Aenean congue mollis placerat. Phasellus aliquam non libero ut varius. Ut sollicitudin maximus ipsum. Nunc enim ante, volutpat id mi eleifend, porta posuere tortor. Cras eros turpis, pellentesque ornare sollicitudin eu, egestas nec eros. Curabitur pharetra ante nec purus ornare ornare. Morbi id sapien volutpat, placerat velit non, placerat lacus. Cras interdum sit amet sapien vel tincidunt.
                                            </li>
                                            <li>Curabitur fringilla velit at sem lacinia laoreet. Integer cursus erat in ipsum imperdiet, vitae feugiat elit pretium. Quisque diam elit, varius iaculis ullamcorper sed, commodo sit amet metus. Suspendisse et quam imperdiet, convallis odio ac, sollicitudin magna. Vestibulum porttitor faucibus vestibulum. Fusce dictum aliquet ante, sed faucibus ligula tincidunt laoreet. Quisque eleifend imperdiet ex eu feugiat. Donec maximus feugiat leo egestas ornare. Duis vitae est quis diam malesuada interdum. Ut tellus erat, porttitor sed tincidunt in, lobortis et felis. Maecenas id condimentum sem. Donec laoreet est arcu, eget pharetra mi rutrum at.</li>
                                            <li>
                                                Phasellus nibh felis, finibus et fringilla vitae, lobortis eu dui. Suspendisse et massa convallis, laoreet tortor ut, efficitur magna. Donec facilisis eu velit eu malesuada. Nullam bibendum ultricies molestie. Phasellus sed scelerisque sapien, in varius tortor. Quisque pellentesque mattis felis sit amet maximus. In vel est vel tellus pretium venenatis. Curabitur dolor nunc, luctus at suscipit quis, fermentum et odio. Aliquam et egestas arcu, eget venenatis quam. Phasellus turpis sem, venenatis in diam eu, ornare volutpat lectus. Nulla tempor nec ex eu tincidunt. Sed aliquam, arcu vitae gravida tincidunt, velit nisi faucibus odio, quis finibus nulla magna vel erat. Sed vel tempor nibh, porttitor nisi. Proin facilisis dui vitae tellus efficitur consequat. Nullam tincidunt felis dui, non porttitor nisi consequat sit amet.
                                            </li>
                                        </ul>
                                    </Tab>
                                    <Tab eventKey="Additional Information" title="Additional Information">
                                        <ul className="desdetail">
                                            <li>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam ex, pellentesque vel vestibulum vitae, elementum consectetur libero. Ut vitae lobortis ex. Aenean congue mollis placerat. Phasellus aliquam non libero ut varius. Ut sollicitudin maximus ipsum. Nunc enim ante, volutpat id mi eleifend, porta posuere tortor. Cras eros turpis, pellentesque ornare sollicitudin eu, egestas nec eros. Curabitur pharetra ante nec purus ornare ornare. Morbi id sapien volutpat, placerat velit non, placerat lacus. Cras interdum sit amet sapien vel tincidunt.
                                            </li>
                                            <li>Curabitur fringilla velit at sem lacinia laoreet. Integer cursus erat in ipsum imperdiet, vitae feugiat elit pretium. Quisque diam elit, varius iaculis ullamcorper sed, commodo sit amet metus. Suspendisse et quam imperdiet, convallis odio ac, sollicitudin magna. Vestibulum porttitor faucibus vestibulum. Fusce dictum aliquet ante, sed faucibus ligula tincidunt laoreet. Quisque eleifend imperdiet ex eu feugiat. Donec maximus feugiat leo egestas ornare. Duis vitae est quis diam malesuada interdum. Ut tellus erat, porttitor sed tincidunt in, lobortis et felis. Maecenas id condimentum sem. Donec laoreet est arcu, eget pharetra mi rutrum at.</li>
                                            <li>
                                                Phasellus nibh felis, finibus et fringilla vitae, lobortis eu dui. Suspendisse et massa convallis, laoreet tortor ut, efficitur magna. Donec facilisis eu velit eu malesuada. Nullam bibendum ultricies molestie. Phasellus sed scelerisque sapien, in varius tortor. Quisque pellentesque mattis felis sit amet maximus. In vel est vel tellus pretium venenatis. Curabitur dolor nunc, luctus at suscipit quis, fermentum et odio. Aliquam et egestas arcu, eget venenatis quam. Phasellus turpis sem, venenatis in diam eu, ornare volutpat lectus. Nulla tempor nec ex eu tincidunt. Sed aliquam, arcu vitae gravida tincidunt, velit nisi faucibus odio, quis finibus nulla magna vel erat. Sed vel tempor nibh, porttitor nisi. Proin facilisis dui vitae tellus efficitur consequat. Nullam tincidunt felis dui, non porttitor nisi consequat sit amet.
                                            </li>
                                        </ul>
                                    </Tab>

                                    <Tab eventKey="Size Guide" title="Size Guide">
                                        <ul className="desdetail">
                                            <li>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam ex, pellentesque vel vestibulum vitae, elementum consectetur libero. Ut vitae lobortis ex. Aenean congue mollis placerat. Phasellus aliquam non libero ut varius. Ut sollicitudin maximus ipsum. Nunc enim ante, volutpat id mi eleifend, porta posuere tortor. Cras eros turpis, pellentesque ornare sollicitudin eu, egestas nec eros. Curabitur pharetra ante nec purus ornare ornare. Morbi id sapien volutpat, placerat velit non, placerat lacus. Cras interdum sit amet sapien vel tincidunt.
                                            </li>
                                            <li>Curabitur fringilla velit at sem lacinia laoreet. Integer cursus erat in ipsum imperdiet, vitae feugiat elit pretium. Quisque diam elit, varius iaculis ullamcorper sed, commodo sit amet metus. Suspendisse et quam imperdiet, convallis odio ac, sollicitudin magna. Vestibulum porttitor faucibus vestibulum. Fusce dictum aliquet ante, sed faucibus ligula tincidunt laoreet. Quisque eleifend imperdiet ex eu feugiat. Donec maximus feugiat leo egestas ornare. Duis vitae est quis diam malesuada interdum. Ut tellus erat, porttitor sed tincidunt in, lobortis et felis. Maecenas id condimentum sem. Donec laoreet est arcu, eget pharetra mi rutrum at.</li>
                                            <li>
                                                Phasellus nibh felis, finibus et fringilla vitae, lobortis eu dui. Suspendisse et massa convallis, laoreet tortor ut, efficitur magna. Donec facilisis eu velit eu malesuada. Nullam bibendum ultricies molestie. Phasellus sed scelerisque sapien, in varius tortor. Quisque pellentesque mattis felis sit amet maximus. In vel est vel tellus pretium venenatis. Curabitur dolor nunc, luctus at suscipit quis, fermentum et odio. Aliquam et egestas arcu, eget venenatis quam. Phasellus turpis sem, venenatis in diam eu, ornare volutpat lectus. Nulla tempor nec ex eu tincidunt. Sed aliquam, arcu vitae gravida tincidunt, velit nisi faucibus odio, quis finibus nulla magna vel erat. Sed vel tempor nibh, porttitor nisi. Proin facilisis dui vitae tellus efficitur consequat. Nullam tincidunt felis dui, non porttitor nisi consequat sit amet.
                                            </li>
                                        </ul>
                                    </Tab>
                                    <Tab eventKey="Delivery Return" title="Delivery Return">
                                        <ul className="desdetail">
                                            <li>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam ex, pellentesque vel vestibulum vitae, elementum consectetur libero. Ut vitae lobortis ex. Aenean congue mollis placerat. Phasellus aliquam non libero ut varius. Ut sollicitudin maximus ipsum. Nunc enim ante, volutpat id mi eleifend, porta posuere tortor. Cras eros turpis, pellentesque ornare sollicitudin eu, egestas nec eros. Curabitur pharetra ante nec purus ornare ornare. Morbi id sapien volutpat, placerat velit non, placerat lacus. Cras interdum sit amet sapien vel tincidunt.
                                            </li>
                                            <li>Curabitur fringilla velit at sem lacinia laoreet. Integer cursus erat in ipsum imperdiet, vitae feugiat elit pretium. Quisque diam elit, varius iaculis ullamcorper sed, commodo sit amet metus. Suspendisse et quam imperdiet, convallis odio ac, sollicitudin magna. Vestibulum porttitor faucibus vestibulum. Fusce dictum aliquet ante, sed faucibus ligula tincidunt laoreet. Quisque eleifend imperdiet ex eu feugiat. Donec maximus feugiat leo egestas ornare. Duis vitae est quis diam malesuada interdum. Ut tellus erat, porttitor sed tincidunt in, lobortis et felis. Maecenas id condimentum sem. Donec laoreet est arcu, eget pharetra mi rutrum at.</li>
                                            <li>
                                                Phasellus nibh felis, finibus et fringilla vitae, lobortis eu dui. Suspendisse et massa convallis, laoreet tortor ut, efficitur magna. Donec facilisis eu velit eu malesuada. Nullam bibendum ultricies molestie. Phasellus sed scelerisque sapien, in varius tortor. Quisque pellentesque mattis felis sit amet maximus. In vel est vel tellus pretium venenatis. Curabitur dolor nunc, luctus at suscipit quis, fermentum et odio. Aliquam et egestas arcu, eget venenatis quam. Phasellus turpis sem, venenatis in diam eu, ornare volutpat lectus. Nulla tempor nec ex eu tincidunt. Sed aliquam, arcu vitae gravida tincidunt, velit nisi faucibus odio, quis finibus nulla magna vel erat. Sed vel tempor nibh, porttitor nisi. Proin facilisis dui vitae tellus efficitur consequat. Nullam tincidunt felis dui, non porttitor nisi consequat sit amet.
                                            </li>
                                        </ul>
                                    </Tab>
                                    <Tab eventKey="Review" title="Review">
                                        <ul className="desdetail">
                                            <li>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam ex, pellentesque vel vestibulum vitae, elementum consectetur libero. Ut vitae lobortis ex. Aenean congue mollis placerat. Phasellus aliquam non libero ut varius. Ut sollicitudin maximus ipsum. Nunc enim ante, volutpat id mi eleifend, porta posuere tortor. Cras eros turpis, pellentesque ornare sollicitudin eu, egestas nec eros. Curabitur pharetra ante nec purus ornare ornare. Morbi id sapien volutpat, placerat velit non, placerat lacus. Cras interdum sit amet sapien vel tincidunt.
                                            </li>
                                            <li>Curabitur fringilla velit at sem lacinia laoreet. Integer cursus erat in ipsum imperdiet, vitae feugiat elit pretium. Quisque diam elit, varius iaculis ullamcorper sed, commodo sit amet metus. Suspendisse et quam imperdiet, convallis odio ac, sollicitudin magna. Vestibulum porttitor faucibus vestibulum. Fusce dictum aliquet ante, sed faucibus ligula tincidunt laoreet. Quisque eleifend imperdiet ex eu feugiat. Donec maximus feugiat leo egestas ornare. Duis vitae est quis diam malesuada interdum. Ut tellus erat, porttitor sed tincidunt in, lobortis et felis. Maecenas id condimentum sem. Donec laoreet est arcu, eget pharetra mi rutrum at.</li>
                                            <li>
                                                Phasellus nibh felis, finibus et fringilla vitae, lobortis eu dui. Suspendisse et massa convallis, laoreet tortor ut, efficitur magna. Donec facilisis eu velit eu malesuada. Nullam bibendum ultricies molestie. Phasellus sed scelerisque sapien, in varius tortor. Quisque pellentesque mattis felis sit amet maximus. In vel est vel tellus pretium venenatis. Curabitur dolor nunc, luctus at suscipit quis, fermentum et odio. Aliquam et egestas arcu, eget venenatis quam. Phasellus turpis sem, venenatis in diam eu, ornare volutpat lectus. Nulla tempor nec ex eu tincidunt. Sed aliquam, arcu vitae gravida tincidunt, velit nisi faucibus odio, quis finibus nulla magna vel erat. Sed vel tempor nibh, porttitor nisi. Proin facilisis dui vitae tellus efficitur consequat. Nullam tincidunt felis dui, non porttitor nisi consequat sit amet.
                                            </li>
                                        </ul>
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Description