import React from "react";
import Productview from "./Productview/Productview";
import Description from "./Description/Description";
import RelatedProduct from "../relatedProduct/RelatedProduct";
import { Container } from "react-bootstrap";
import Title from "../layout/Title";

const Produtviewpage = () => {
  return (
    <>
      <Productview />
      <Description />
      <section>
        <Container>
          <Title title_main={"Related Products"} />
          <RelatedProduct />
        </Container>
      </section>
    </>
  );
};

export default Produtviewpage;
