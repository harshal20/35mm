import Header from "./Header/Header";
import Admin from "./admin/Admin";
import Footer from "./Footer/Footer";
import { Logic } from "./logic/Logic";
import { RouteToTop } from "./logic/Logic";

export { Logic, Header, RouteToTop, Footer, Admin };
