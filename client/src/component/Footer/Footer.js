import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import { discover, footerlogo, mastercard, paypal, visa } from '../../assets/images'

const Footer = () => {
    return (
        <>
            <footer>
                <Container>
                    <Row>
                        <Col lg={3}>
                            <div className="firstcol">
                                <Link to="/">   <img src={footerlogo} alt="" /></Link>
                                <p>We are a discovery marketplace platform. We provide a new experience of shopping.</p>
                                <ul className="icons">
                                    <li><Link to={"/"}>
                                        <svg id="facebook-fill_6_" data-name="facebook-fill (6)" xmlns="http://www.w3.org/2000/svg" width="18.333" height="18.333" viewBox="0 0 18.333 18.333">
                                            <path id="Path_87" data-name="Path 87" d="M0,0H18.333V18.333H0Z" fill="rgba(0,0,0,0)" />
                                            <path id="Path_88" data-name="Path 88" d="M12.347,10.785h1.91l.764-3.056H12.347V6.2c0-.787,0-1.528,1.528-1.528h1.146V2.107A21.516,21.516,0,0,0,12.838,2,3.312,3.312,0,0,0,9.292,5.59V7.729H7v3.056H9.292v6.493h3.056Z" transform="translate(-1.653 -0.472)" fill="#8392a5" />
                                        </svg>

                                    </Link></li>
                                    <li><Link to={"/"}><svg id="linkedin-fill_4_" data-name="linkedin-fill (4)" xmlns="http://www.w3.org/2000/svg" width="18.333" height="18.333" viewBox="0 0 18.333 18.333">
                                        <path id="Path_81" data-name="Path 81" d="M0,0H18.333V18.333H0Z" fill="none" />
                                        <path id="Path_82" data-name="Path 82" d="M6,4.528A1.528,1.528,0,1,1,4.469,3,1.528,1.528,0,0,1,6,4.528Zm.046,2.658H2.986V16.75H6.041Zm4.828,0H7.829V16.75h3.01V11.731c0-2.8,3.644-3.056,3.644,0V16.75H17.5V10.692c0-4.713-5.393-4.537-6.661-2.223Z" transform="translate(-0.694 -0.708)" fill="#8392a5" />
                                    </svg>
                                    </Link></li>
                                    <li><Link to={"/"}><svg id="instagram-line_1_" data-name="instagram-line (1)" xmlns="http://www.w3.org/2000/svg" width="18.333" height="18.333" viewBox="0 0 18.333 18.333">
                                        <path id="Path_83" data-name="Path 83" d="M0,0H18.333V18.333H0Z" fill="none" />
                                        <path id="Path_84" data-name="Path 84" d="M9.639,7.347A2.292,2.292,0,1,0,11.93,9.639,2.292,2.292,0,0,0,9.639,7.347Zm0-1.528A3.819,3.819,0,1,1,5.819,9.639,3.819,3.819,0,0,1,9.639,5.819ZM14.6,5.628a.955.955,0,1,1-.955-.955A.955.955,0,0,1,14.6,5.628Zm-4.965-2.1c-1.89,0-2.2.005-3.078.044a4.15,4.15,0,0,0-1.373.254A2.383,2.383,0,0,0,3.825,5.188a4.144,4.144,0,0,0-.253,1.373c-.04.843-.044,1.138-.044,3.078,0,1.89.005,2.2.044,3.078a4.166,4.166,0,0,0,.253,1.373,2.375,2.375,0,0,0,1.361,1.362,4.158,4.158,0,0,0,1.375.254c.843.04,1.138.044,3.078.044,1.89,0,2.2-.005,3.078-.044a4.175,4.175,0,0,0,1.373-.253,2.231,2.231,0,0,0,.825-.536,2.209,2.209,0,0,0,.538-.825,4.176,4.176,0,0,0,.254-1.375c.04-.843.044-1.138.044-3.078,0-1.89-.005-2.2-.044-3.078a4.158,4.158,0,0,0-.254-1.373,2.224,2.224,0,0,0-.537-.825,2.2,2.2,0,0,0-.825-.538,4.148,4.148,0,0,0-1.373-.253C11.873,3.532,11.578,3.528,9.639,3.528ZM9.639,2c2.075,0,2.334.008,3.149.046a5.627,5.627,0,0,1,1.855.355A3.724,3.724,0,0,1,16,3.282a3.749,3.749,0,0,1,.881,1.354,5.643,5.643,0,0,1,.355,1.855c.036.814.046,1.073.046,3.149s-.008,2.334-.046,3.149a5.647,5.647,0,0,1-.355,1.855,3.9,3.9,0,0,1-2.234,2.234,5.643,5.643,0,0,1-1.855.355c-.814.036-1.073.046-3.149.046S7.3,17.27,6.49,17.232a5.647,5.647,0,0,1-1.855-.355A3.9,3.9,0,0,1,2.4,14.642a5.623,5.623,0,0,1-.355-1.855C2.01,11.973,2,11.714,2,9.639S2.008,7.3,2.046,6.49A5.623,5.623,0,0,1,2.4,4.635,3.9,3.9,0,0,1,4.635,2.4,5.623,5.623,0,0,1,6.49,2.046C7.3,2.01,7.563,2,9.639,2Z" transform="translate(-0.472 -0.472)" fill="#8392a5" />
                                    </svg>
                                    </Link></li>
                                    <li><Link to={"/"}><svg id="twitter-line_1_" data-name="twitter-line (1)" xmlns="http://www.w3.org/2000/svg" width="18.333" height="18.333" viewBox="0 0 18.333 18.333">
                                        <path id="Path_85" data-name="Path 85" d="M0,0H18.333V18.333H0Z" fill="none" />
                                        <path id="Path_86" data-name="Path 86" d="M12.017,5.077A2.215,2.215,0,0,0,9.8,7.253l-.021,1.2a.458.458,0,0,1-.519.445L8.068,8.739A8.943,8.943,0,0,1,3.554,6.6a5.012,5.012,0,0,0,2.584,5.631l1.334.839a.458.458,0,0,1,.026.759l-1.216.888a7.915,7.915,0,0,0,1.98-.1c3.6-.72,6-3.431,6-7.9a2.307,2.307,0,0,0-2.246-1.635ZM8.274,7.224a3.743,3.743,0,0,1,6.405-2.563,2.946,2.946,0,0,0,2.039-.493,5.521,5.521,0,0,1-.927,2.544c0,5.838-3.588,8.676-7.229,9.4-2.5.5-6.126-.32-7.167-1.406a8.82,8.82,0,0,0,3.929-1.184c-1.053-.694-5.246-3.162-2.491-9.8A10.729,10.729,0,0,0,6.767,6.814a4.49,4.49,0,0,0,1.507.411Z" transform="translate(-0.329 -0.838)" fill="#8392a5" />
                                    </svg></Link>
                                    </li>
                                </ul>
                            </div>
                        </Col>

                        <Col lg={6}>
                            <Row className='footerrow'>
                                <Col lg={2}>
                                    <div className="featured">
                                        <h4>Featured</h4>
                                        <ul className="homesdetail">
                                            <li><Link to={"/"}>Home</Link></li>
                                            <li><Link to={"/"}>Men</Link></li>
                                            <li><Link to={"/"}>Women</Link></li>
                                            <li><Link to={"/"}>Kids</Link></li>
                                            <li><Link to={"/"}>Accessories</Link></li>
                                        </ul>
                                    </div>
                                </Col>
                                <Col lg={2}>
                                    <div className="shoes ">
                                        <ul className="homesdetail mt-20">
                                            <li><Link to={"/"}>Shoes</Link></li>
                                            <li><Link to={"/"}>Jewelries</Link></li>
                                            <li><Link to={"/"}>Bags</Link></li>
                                            <li><Link to={"/"}>Sunglasses </Link></li>
                                        </ul>
                                    </div>
                                </Col>

                                <Col lg={2}>
                                    <div className="featured">
                                        <h4 >Information</h4>
                                        <ul className="homesdetail">
                                            <li><Link to={"/"}>About Us</Link></li>
                                            <li><Link to={"/"}>Contact Us</Link></li>
                                            <li><Link to={"/"}>My Order</Link></li>
                                            <li><Link to={"/"}>Our Policies</Link></li>
                                            <li><Link to={"/"}>Returns & Exchanges</Link></li>
                                            <li><Link to={"/"}>Blog </Link></li>
                                        </ul>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <Col lg={3}>
                            <div className="join">
                                <h4>Join Our Newsletter Now</h4>
                                <p>Sign up to get  email updates about our latest shops and special offers! </p>
                                <div className="email">
                                    <input type="text" placeholder='Enter Email' />
                                    <Link to="">Subscribe</Link>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
                <div className="copyright">
                    <div className="copy">
                        <p>© Copyright 35MM 2020. All Right Reserved.</p>
                        <ul className="visaimg">
                            <li><Link to={"/"}><img src={visa} alt="" /></Link></li>
                            <li><Link to={"/"}><img src={paypal} alt="" /></Link></li>
                            <li><Link to={"/"}><img src={mastercard} alt="" /></Link></li>
                            <li><Link to={"/"}><img src={discover} alt="" /></Link></li>
                        </ul>
                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer