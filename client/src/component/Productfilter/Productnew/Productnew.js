import React, { useEffect } from "react";
import { Container, Col, Breadcrumb, Row } from "react-bootstrap";
import "./Productnew.css";
import { Link } from "react-router-dom";
import color4 from "../../../assets/images/color4.png";
import color5 from "../../../assets/images/color5.png";
import color6 from "../../../assets/images/color6.png";
import color7 from "../../../assets/images/color7.png";
import feaimg1 from "../../../assets/images/feaimg1.png";
import feaimg2 from "../../../assets/images/feaimg2.png";
import feaimg3 from "../../../assets/images/feaimg3.png";
import { useState } from "react";
import { Rating, Slider } from "@mui/material";
import Pagination from "https://cdn.skypack.dev/rc-pagination@3.1.15";
import Swiper from "swiper";
import { SwiperSlide } from "swiper/react";
import ReactPaginate from "react-paginate";
import { AiOutlineEye } from "react-icons/ai";
import { TbAdjustments } from "react-icons/tb";
import { FiShoppingBag, FiHeart } from "react-icons/fi";

const Productsnew = () => {

  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [sortState, setSortState] = useState("none");
  const [item, setItem] = useState(null);

  // price
  const [value, setValue] = useState([50, 1000]);

  // category
  const [category, setCategory] = useState("all");

  // pagination
  const [page, setPage] = useState(1);

  const clothes = data.filter((item) => item.category === "Men’s Clothes");

  const addClass = useRef(null);

  const handleAdd = () => {
    addClass.current.classList.add("list");
  };

  const handleRemove = () => {
    addClass.current.classList.remove("list");
  };

  useEffect(() => {
    setIsLoading(true);
    async function fetchData() {
      const response = await fetch(
        "http://localhost:5000/api/product/allProducts"
      )
        .then((response) => response.json())

        .then((res) => {
          setData((prevState) => [...res.data]);
          setIsLoading(false);
        })
        .catch((err) => console.log(err));

    }
    fetchData();

    setItem(clothes);
  }, []);

  // pagination
  useEffect(() => {
    const endOffset = imagesOffset + 8;
    setCurrentImages(data.slice(imagesOffset, endOffset));
    setPageCount(Math.ceil(data.length / 8));
  }, [data, imagesOffset]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * 8) % data.length;
    setImagesOffset(newOffset);
  };

  const sortMethods = {
    none: { method: (a, b) => null },
    ascending: { method: (a, b) => (a.price > b.price ? 1 : -1) },
    descending: { method: (a, b) => (a.price < b.price ? 1 : -1) },
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // handle category button click
  const handlearCategory = (category) => {
    setCategory(category);
    console.log(category)
  };

  return (
    <>
      <section className="productfilterdetails">
        <div className="womrncrub">
          <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item active> Woman</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <Container>
          <Row>
            <Col lg={3}>
              <div className="category">
                <h3> Categories</h3>
                <ul className="mensfasion">
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("all")}
                    >
                      All
                    </button>
                    <span>({data.length})</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Men’s Clothes")}
                    >
                      Men’s Clothes
                    </button>
                    <span>(({category.length}))</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Women’s Clothes")}
                    >
                      Women’s Clothes
                    </button>
                    <span>({data.length})</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Kids’ Clothes")}
                    >
                      Kids’ Clothes
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Accessories")}
                    >
                      Accessories
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Essentials")}
                    >
                      Essentials
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Fall")}
                    >
                      Fall
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Winter")}
                    >
                      Winter
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Spring")}
                    >
                      Spring
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Summer")}
                    >
                      Summer
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Beauty and Health")}
                    >
                      Beauty and Health
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Home Decor")}
                    >
                      Home Decor
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Clothes")}
                    >
                      Clothes
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Shoes")}
                    >
                      Shoes
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Underwear")}
                    >
                      Underwear
                    </button>
                    <span>(22)</span>
                  </li>
                  <li>
                    <button
                      className="category_item"
                      onClick={() => handlearCategory("Miscellaneous")}
                    >
                      Miscellaneous
                    </button>
                    <span>(22)</span>
                  </li>
                </ul>
                <div className="colordetailcom">
                  <h4>Color</h4>
                  <span>
                    <Link>
                      <img src={color4} alt="" />
                    </Link>
                  </span>
                  <span>
                    <Link>
                      <img src={color5} alt="" />
                    </Link>
                  </span>
                  <span>
                    <Link>
                      <img src={color6} alt="" />
                    </Link>
                  </span>
                  <span>
                    <Link>
                      <img src={color7} alt="" />
                    </Link>
                  </span>
                </div>
                <div className="colordetailcom">
                  <h4>Filter by Prices</h4>
                  <div className="pricespan">
                    <Slider
                      value={value}
                      onChange={handleChange}
                      valueLabelDisplay="auto"
                      aria-labelledby="price-filter-slider"
                      max={1000}
                      min={0}
                    />
                    <h4>{`Price: $${value[0]} - $${value[1]}`}</h4>
                  </div>
                </div>
                <div className="colordetailcom">
                  <h4>Size</h4>
                  <div className="sizesdetail">
                    <input type="text" value="L" />
                    <input type="text" value="M" />
                    <input type="text" value="S" />
                    <input type="text" value="XL" />
                    <input type="text" value="XS" />
                    <input type="text" value="XXL" />
                  </div>
                </div>
                <div className="colordetailcom">
                  <h3>Featured Products</h3>
                  <ul className="jeans">
                    <li className="rateproduct">
                      <img src={feaimg1} alt="" />
                      <div className="jeans">
                        <h4>Low waist ripped jeans</h4>
                        <span>
                          <svg
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                              fill="#FF7D51"
                            />
                          </svg>
                          5.0
                        </span>
                        <p>$190.00</p>
                      </div>
                    </li>
                    <li className="rateproduct">
                      <img src={feaimg2} alt="" />
                      <div className="jeans">
                        <h4>Low waist ripped jeans</h4>
                        <span>
                          <svg
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                              fill="#FF7D51"
                            />
                          </svg>
                          5.0
                        </span>
                        <p>$190.00</p>
                      </div>
                    </li>
                    <li className="rateproduct">
                      <img src={feaimg3} alt="" />
                      <div className="jeans">
                        <h4>Low waist ripped jeans</h4>
                        <span>
                          <svg
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z"
                              fill="#FF7D51"
                            />
                          </svg>
                          5.0
                        </span>
                        <p>$190.00</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </Col>
            <Col lg={9}>
              <div className="sertdetail">
                <ul className="sertdetail ">
                  <span>Sort By</span>

                  <select defaultValue={'DEFAULT'} onChange={(e) => setSortState(e.target.value)}>
                    <option value="DEFAULT" disabled>Price</option>
                    <option value="ascending">Price low to high</option>
                    <option value="descending">Price high to low</option>
                  </select>

                  <li className="grid">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M14 10H10V14H14V10ZM16 10V14H19V10H16ZM14 19V16H10V19H14ZM16 19H19V16H16V19ZM14 5H10V8H14V5ZM16 5V8H19V5H16ZM8 10H5V14H8V10ZM8 19V16H5V19H8ZM8 5H5V8H8V5ZM4 3H20C20.5523 3 21 3.44772 21 4V20C21 20.5523 20.5523 21 20 21H4C3.44772 21 3 20.5523 3 20V4C3 3.44772 3.44772 3 4 3Z"
                        fill="#1190CB"
                      />
                    </svg>
                    Grid
                  </li>
                  <li className="grid" onClick={handleClick}>
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M11 4H21V6H11V4ZM11 8H17V10H11V8ZM11 14H21V16H11V14ZM11 18H17V20H11V18ZM3 4H9V10H3V4ZM5 6V8H7V6H5ZM3 14H9V20H3V14ZM5 16V18H7V16H5Z"
                        fill="#3C4858"
                      />
                    </svg>
                    List
                  </li>
                </ul>
                <div className="showing">
                  <h4>
                    {/* Showing 01-12 of   {data.length} Results */}
                    <Pagination
                      className="pagination-data mainpage"
                      showTotal={(total, range) => `Showing ${range[0]}-${range[1]} of ${total} Results`}
                      onChange={PaginationChange}
                      total={data.length}
                      current={current}
                      pageSize={size}
                    // showSizeChanger={false}
                    // itemRender={PrevNextArrow}
                    // onShowSizeChange={PerPageChange}
                    />
                  </h4>
                </div>
              </div>
              <div className="longdetail">
                {/* <Title title_main={'Related Products'} /> */}
                <div className="live_releted_product">
                  <ul ref={addClass} className="jeans jeansmain">
                    {data
                      .filter(
                        (product) =>
                          product.price >= value[0] && product.price <= value[1]
                      )
                      .filter((product) => {
                        if (category === "all") {
                          return true;
                        } else {
                          return product.category === category;
                        }
                      })
                      .sort(sortMethods[sortState].method)
                      .map((value, index) => {
                        return (
                          <>
                            <li className="rateproduct" key={index}>
                              <div className="live_streaming_slider">
                                <div
                                  className="contact_store_product_inner"
                                  key={index}
                                >
                                  <Link to={"/"}>
                                    <div className="contact_store_product_img">
                                      <img
                                        src={`http://localhost:5000/${value.thumbnail.path}`}
                                        alt=""
                                      />
                                      <div className="contact_details">
                                        <span className="contact_details_icons">
                                          <AiOutlineEye />
                                        </span>
                                        <span className="contact_details_icons">
                                          <FiShoppingBag />
                                        </span>
                                        <span className="contact_details_icons">
                                          <FiHeart />
                                        </span>
                                        <span className="contact_details_icons">
                                          <TbAdjustments />
                                        </span>
                                      </div>
                                    </div>
                                    <h4 className="contact_product_head">
                                      {value.title}
                                    </h4>
                                    <span className="contact_product_rating">
                                      <Rating
                                        name="half-rating-read"
                                        defaultValue={5.0}
                                        precision={4}
                                        readOnly
                                      />
                                      <span className="contact_product_point">
                                        {value.rating}
                                      </span>
                                    </span>
                                    <p className="contact_product_price">
                                      {value.price}
                                    </p>
                                  </Link>
                                </div>
                              </li>
                            </>
                          );
                        })}
                  </ul>
                  <div className="pagination">
                    {/* <ReactPaginate
                      breakLabel="..."
                      nextLabel="next >"
                      onPageChange={handlePageClick}
                      pageRangeDisplayed={5}
                      pageCount={pageCount}
                      previousLabel="< previous"
                      renderOnZeroPageCount={null}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      containerClassName={"pagination"}
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      activeClassName={"active"}
                    /> */}
                    <Pagination
                      className="pagination-data subpagi"
                      showTotal={(total, range) => `Showing ${range[0]}-${range[1]} of ${total}`}
                      onChange={PaginationChange}
                      total={data.length}
                      current={current}
                      pageSize={size}
                      showSizeChanger={false}
                      itemRender={PrevNextArrow}
                      onShowSizeChange={PerPageChange}
                    />
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
export default Productsnew;
