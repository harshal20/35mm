import axios from "axios";
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
// import config from "./config";

function Main() {
  const [images, setImages] = useState([]);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [currentImages, setCurrentImages] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [imagesOffset, setImagesOffset] = useState(0);

  useEffect(() => {
    const endOffset = imagesOffset + 8;
    setCurrentImages(images.slice(imagesOffset, endOffset));
    setPageCount(Math.ceil(images.length / 8));
  }, [images, imagesOffset]);

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`https://api.unsplash.com/photos/?client_id/{page}`)
      .then((res) => {
        setImages((prevState) => [...res.data]);
        setIsLoading(false);
        console.log(res.data);
        return res.data;
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
      });
  }, []);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * 8) % images.length;
    setImagesOffset(newOffset);
  };

  return (
    <div>
      <div className="pagination">
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          <>
            <button
              disabled={page === 1}
              onClick={() => setPage((prevState) => prevState - 1)}
            >
              Prev
            </button>
            <p>{page}</p>
            <button onClick={() => setPage((prevState) => prevState + 1)}>
              Next
            </button>
          </>
        )}
      </div>
      <div className="App">
        {images?.map((image, i) => {
          return (
            <div className="img-wrapper" key={i}>
              <img src={image?.urls?.thumb} alt={image.alt_description} />
            </div>
          );
        })}
      </div>
      <div className="pagination">
        <ReactPaginate
          breakLabel="..."
          nextLabel="next >"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          previousLabel="< previous"
          renderOnZeroPageCount={null}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          containerClassName={"pagination"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>
    </div>
  );
}
export default Main;
