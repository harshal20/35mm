import React, { useEffect, useState } from "react";
import "swiper/css";
import "swiper/css/pagination";
import { Autoplay, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { Link } from "react-router-dom";
import { Rating } from "@mui/material";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import { useDispatch } from "react-redux";
import { ADD } from "../../redux/actions";
import { TbAdjustments } from "react-icons/tb";
import { FiHeart, FiShoppingBag } from "react-icons/fi";
import { AiOutlineEye } from "react-icons/ai";
import { ProUrl } from "../../helper/api";
import axios from "axios";

const RelatedProduct = () => {
  const [indexOfImages, setIndexOfImages] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState([]);
  console.log(data);

  // const product = `http://localhost:5000/${data.thumbnail.path}`;
  // console.log(product);
  // redux dispatch

  const dispatch = useDispatch();

  // const { id } = useParams();

  const openModalAndSetIndex = (index) => {
    setIndexOfImages(index);
    setShowModal(true);
    return;
  };

  // redux send data to useTo dispatch

  const send = (e) => {
    dispatch(ADD(e));
  };

  const fetchData = async () => {
    const response = await fetch(
      "http://localhost:5000/api/product/allProducts"
    )
      .then((response) => response.json())
      .then((res) => {
        setData((prevState) => [...res.data]);
        return res.data;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {showModal && (
        <Lightbox
<<<<<<< HEAD
          mainSrc={data[indexOfImages].thumbnail.name}
          nextSrc={data[(indexOfImages + 1) % data.length]}
          prevSrc={data[(indexOfImages + data - 1) % data.length]}
          onCloseRequest={() => setShowModal(false)}
          onMovePrevRequest={() =>
            setIndexOfImages((indexOfImages + data - 1) % data.length)
          }
          onMoveNextRequest={() =>
            setIndexOfImages((indexOfImages + data.length + 1) % data.length)
=======
          mainSrc={Contact_product.contact_product_content[indexOfImages].image}
          nextSrc={
            Contact_product.contact_product_content[
            (indexOfImages + 1) %
            Contact_product.contact_product_content.length
            ]
          }
          prevSrc={
            Contact_product.contact_product_content[
            (indexOfImages +
              Contact_product.contact_product_content.length -
              1) %
            Contact_product.contact_product_content.length
            ]
          }
          onCloseRequest={() => setShowModal(false)}
          onMovePrevRequest={() =>
            setIndexOfImages(
              (indexOfImages +
                Contact_product.contact_product_content.length -
                1) %
              Contact_product.contact_product_content.length
            )
          }
          onMoveNextRequest={() =>
            setIndexOfImages(
              (indexOfImages +
                Contact_product.contact_product_content.length +
                1) %
              Contact_product.contact_product_content.length
            )
>>>>>>> 20d9e83bcb07512087b182b1fac9565ba948e884
          }
        />
      )}
      {/* <>
      {showModal && (
        <Lightbox
          mainSrc={data[indexOfImages].image}
          nextSrc={data[(indexOfImages + 1) % data.length]}
          prevSrc={data[(indexOfImages + data - 1) % data.length]}
          onCloseRequest={() => setShowModal(false)}
          onMovePrevRequest={() =>
            setIndexOfImages((indexOfImages + data - 1) % data.length)
          }
          onMoveNextRequest={() =>
            setIndexOfImages((indexOfImages + data.length + 1) % data.length)
          }
        />
      )} */}
      <div className="live_releted_product">
        <div className="live_streaming_slider">
          <Swiper
            slidesPerView={1}
            spaceBetween={30}
            pagination={{
              dynamicBullets: true,
            }}
            loop={true}
            // autoplay={{
            //   delay: 2500,
            //   disableOnInteraction: false,
            // }}
            breakpoints={{
              640: {
                slidesPerView: 2,
                spaceBetween: 20,
              },
              768: {
                slidesPerView: 4,
                spaceBetween: 40,
              },
              1024: {
                slidesPerView: 4,
                spaceBetween: 30,
              },
            }}
            modules={[Autoplay, Pagination]}
            className="mySwiper"
          >
            {data.map((value, index) => {
              return (
                <SwiperSlide>
                  <div className="contact_store_product_inner" key={index}>
                    <Link to={"/"}>
                      <div className="contact_store_product_img">
                        <img
                          src={`http://localhost:5000/${value.thumbnail.path}`}
                          alt="img"
                        />
                        <div className="contact_details">
                          <Link
                            onClick={() => openModalAndSetIndex(index)}
                            className="contact_details_icons"
                          >
                            <AiOutlineEye />
                          </Link>
                          <Link
                            className="contact_details_icons"
                            onClick={() => send(value)}
                          >
                            <FiShoppingBag />
                          </Link>
                          <Link className="contact_details_icons">
                            <FiHeart />
                          </Link>
                          <Link className="contact_details_icons">
                            <TbAdjustments />
                          </Link>
                        </div>
                      </div>
                      <h4 className="contact_product_head">{value.title}</h4>
                      <span className="contact_product_rating">
                        <Rating
                          name="half-rating-read"
                          defaultValue={5.0}
                          precision={4}
                          readOnly
                        />
                        <span className="contact_product_point">
                          {value.rating}
                        </span>
                      </span>
                      <p className="contact_product_price">${value.price}</p>
                    </Link>
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
      </div>
    </>
  );
};
export default RelatedProduct;
