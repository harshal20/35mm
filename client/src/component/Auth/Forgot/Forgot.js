import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import login from "../../../assets/images/login.png";
import "./Forgot.css";
const Forgot = () => {
  const [loginUser, setLoginUser] = useState();

  const handleSubmit = () => {};

  return (
    <>
      <section className="forgotdetail">
        <Container>
          <Row>
            <Col lg={5}>
              <div className="loginimg orders">
                <img src={login} alt="" />
                <h4>
                  Get access to your Orders, <br />
                  Wishlist and Recommendations
                </h4>
              </div>
            </Col>
            <Col lg={6}>
              <div className="logindetail">
                <h3>Forgot Password ?</h3>
                <h5>No worries, we'll send you reset instructions.</h5>
                <form
                  action=""
                  id="login"
                  method="post"
                  onSubmit={handleSubmit}
                >
                  <div className="emaillogin">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M2.8 3H17.2C17.6418 3 18 3.34822 18 3.77778V16.2222C18 16.6518 17.6418 17 17.2 17H2.8C2.35817 17 2 16.6518 2 16.2222V3.77778C2 3.34822 2.35817 3 2.8 3ZM16.4 6.29622L10.0576 11.8184L3.6 6.27911V15.4444H16.4V6.29622ZM4.0088 4.55556L10.0488 9.73711L16.0016 4.55556H4.0088Z"
                          fill="#1190CB"
                        />
                      </svg>
                    </span>
                    <input
                      type="text"
                      placeholder="Email"
                      name="email"
                      id="email"
                      value={loginUser}
                      onChange={(e) => setLoginUser(e.target.value)}
                    />
                    {/* <p className={`message text-danger`}>{message}</p> */}
                  </div>
                  <div className="signin">
                    <Link to="/reset">
                      {/* <input type="submit" value="Reset password" /> */}
                      Reset password
                    </Link>
                  </div>
                </form>
                <div className="account backnow">
                  <Link className="reginow" to="/login">
                    <span className="leftarrow">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M7.828 10.9997H20V12.9997H7.828L13.192 18.3637L11.778 19.7777L4 11.9997L11.778 4.22168L13.192 5.63568L7.828 10.9997Z"
                          fill="#09121F"
                        />
                      </svg>
                    </span>
                    Back to login
                  </Link>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Forgot;
