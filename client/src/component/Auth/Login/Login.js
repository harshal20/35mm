import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import login from "../../../assets/images/login.png";
import "./login.css";
import axios from "axios";
import { UserUrl } from "../../../helper/api";
import { ErrorToast, SuccessToast } from "../../../helper/Toast";
import Cookies from "js-cookie";

axios.defaults.timeout = 3000;

const Login = () => {
  const [passwordError, setPasswordError] = useState("");
  const [loginUser, setLoginUser] = useState({
    email: "",
    password: "",
  });
  const [message, setMessage] = useState("");
  const navigate = useNavigate();
  const handleValidation = () => {
    if (loginUser.password.length <= 8) {
      setPasswordError("Password must be at least 8 characters long");
    } else if (!/[A-Z]/.test(loginUser.password)) {
      setPasswordError("Password must contain at least one uppercase letter");
    } else if (!/[a-z]/.test(loginUser.password)) {
      setPasswordError("Password must contain at least one lowercase letter");
    } else if (!/\d/.test(loginUser.password)) {
      setPasswordError("Password must contain at least one number");
    } else if (!/[^a-zA-Z\d]/.test(loginUser.password)) {
      setPasswordError("Password must contain at least one special character");
    } else {
      setPasswordError("");
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setLoginUser({
      ...loginUser,
      [name]: value,
    });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = loginUser;
    if (email && password) {
      UserUrl.post("/login", loginUser, {
        Authorization: `Bearer ${Cookies.get("token")}`,
      })
        .then((response) => {
          // token
          const token = response?.data?.token;
          // 1 / 1440 = 1 minute in fraction of a day
          Cookies.set("token", token, { secure: true, expires: 2 });

          axios.interceptors.request.use((config) => {
            const token = Cookies.get("token");
            if (token) {
              config.headers.Authorization = `Bearer ${token}`;
            }
            return config;
          });
          if (response.status === 200) {
            SuccessToast("SuccessFully Login!");
          }
          navigate("/");
          console.log(response);
        })
        .catch((error) => {
          console.error(error);
          ErrorToast("Please Enter Valid Detail");
        });
    } else {
      ErrorToast("This field is required and cannot be empty!");
    }
  };
  return (
    <>
      <section className="signupdetail">
        <Container>
          <Row>
            <Col lg={5}>
              <div className="loginimg orders">
                <img src={login} alt="" />
                <h4>
                  Get access to your Orders, <br />
                  Wishlist and Recommendations
                </h4>
              </div>
            </Col>
            <Col lg={6}>
              <div className="logindetail">
                <h3>Login</h3>
                <form
                  action=""
                  id="login"
                  method="post"
                  onSubmit={handleSubmit}
                >
                  <div className="emaillogin">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M2.8 3H17.2C17.6418 3 18 3.34822 18 3.77778V16.2222C18 16.6518 17.6418 17 17.2 17H2.8C2.35817 17 2 16.6518 2 16.2222V3.77778C2 3.34822 2.35817 3 2.8 3ZM16.4 6.29622L10.0576 11.8184L3.6 6.27911V15.4444H16.4V6.29622ZM4.0088 4.55556L10.0488 9.73711L16.0016 4.55556H4.0088Z"
                          fill="#1190CB"
                        />
                      </svg>
                    </span>
                    <input
                      type="text"
                      placeholder="Email"
                      name="email"
                      id="email"
                      value={loginUser.email}
                      onChange={handleChange}
                    />
                    <p className={`message text-danger`}>{message}</p>
                  </div>
                  <div className="emaillogin">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M5.33333 6.66667V5.85714C5.33333 3.17462 7.42267 1 10 1C12.5773 1 14.6667 3.17462 14.6667 5.85714V6.66667H16.2222C16.6518 6.66667 17 7.0291 17 7.47619V17.1905C17 17.6376 16.6518 18 16.2222 18H3.77778C3.34822 18 3 17.6376 3 17.1905V7.47619C3 7.0291 3.34822 6.66667 3.77778 6.66667H5.33333ZM15.4444 8.28571H4.55556V16.381H15.4444V8.28571ZM9.22222 12.9259C8.61249 12.5595 8.31523 11.8125 8.49745 11.1047C8.67967 10.3969 9.29595 9.90472 10 9.90472C10.7041 9.90472 11.3203 10.3969 11.5026 11.1047C11.6848 11.8125 11.3875 12.5595 10.7778 12.9259V14.7619H9.22222V12.9259ZM6.88889 6.66667H13.1111V5.85714C13.1111 4.06879 11.7182 2.61905 10 2.61905C8.28178 2.61905 6.88889 4.06879 6.88889 5.85714V6.66667Z"
                          fill="#1190CB"
                        />
                      </svg>
                    </span>
                    <input
                      type="password"
                      onPaste={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      onCopy={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      name="password"
                      placeholder="Password"
                      onKeyUp={handleValidation}
                      required
                      value={loginUser.password}
                      onChange={handleChange}
                    />
                    <p className="text-danger">{passwordError}</p>
                  </div>
                  <div className="remember">
                    <Link className="remme">
                      <input type="radio" />
                      <span>Remember Me</span>
                    </Link>
                    <Link className="forgot" to="/forgot">
                      Forgot Password?
                    </Link>
                  </div>
                  <div className="signin">
                    <input type="submit" value="Login" />
                  </div>
                </form>
                <div className="account">
                  <Link className="need">Need an account?</Link>
                  <Link className="reginow" to="/signup">
                    Register Now
                  </Link>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
export default Login;
