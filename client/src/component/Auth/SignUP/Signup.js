import React, { useEffect, useState } from "react";
import { Col, Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import "./signup.css";
import { Link, useNavigate } from "react-router-dom";
import signup from "../../../assets/images/signup.png";
import { UserUrl } from "../../../helper/api";
import { ErrorToast, SuccessToast } from "../../../helper/Toast";

const Signup = () => {
  const navigate = useNavigate();
  const [user, setUser] = useState({
    userName: "",
    email: "",
    password: "",
  });
  const [confirmPassword, setConfirmPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");
  const [userForm, setUserForm] = useState("");
  const [message, setMessage] = React.useState("");
  const [formIsValid, setFormIsValid] = useState(false);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({
      ...user,
      [name]: value,
    });
  };
  const validatePassword = () => {
    if (user.password.length <= 8) {
      setPasswordError("Password must be at least 8 characters long");
    } else if (!/[A-Z]/.test(user.password)) {
      setPasswordError("Password must contain at least one uppercase letter");
    } else if (!/[a-z]/.test(user.password)) {
      setPasswordError("Password must contain at least one lowercase letter");
    } else if (!/\d/.test(user.password)) {
      setPasswordError("Password must contain at least one number");
    } else if (!/[^a-zA-Z\d]/.test(user.password)) {
      setPasswordError("Password must contain at least one special character");
    } else {
      setPasswordError("");
    }
  };
  const validateConfirmPassword = () => {
    if (confirmPassword !== user.password) {
      setConfirmPasswordError("Passwords do not match");
    } else {
      setConfirmPasswordError("");
    }
  };
  const username = /^[A-Za-z]+$/;
  const regEx =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{3,}))$/;
  const handleValidation = () => {
    if (username.test(user.userName)) {
      setUserForm("");
    } else if (!username.test(user.userName) && user.userName !== "") {
      setUserForm("Invalid username");
    } else {
      setUserForm("");
    }
    if (regEx.test(user.email)) {
      setMessage("");
    } else if (!regEx.test(user.email) && user.email !== "") {
      setMessage("Invalid email");
    } else {
      setMessage("");
    }
  };
  const handleSend = (e) => {
    e.preventDefault();
    const { userName, email, password } = user;
    if (userName && email && password && confirmPassword) {
      UserUrl.post("/register", user)
        .then((res) => {
          console.log(res.data);
          setUser({
            userName: "",
            email: "",
            password: "",
          });
          setConfirmPassword("");
          SuccessToast("Register success!");
          // setTimeout(() => {
          //   navigate("/");
          // }, 3000);
        })
        .catch((error) => {
          if (error.response.data.keyPattern.email === 1) {
            ErrorToast("Email is already Regitered..!");
          }
          console.error(error);
        });
    } else {
      ErrorToast("Required All fields!");
    }
  };
  useEffect(() => {
    const isFormValid =
      user.userName !== "" &&
      user.email !== "" &&
      user.password !== "" &&
      user.password === confirmPassword;
    setFormIsValid(isFormValid);
  }, [user, confirmPassword]);
  return (
    <>
      <section className="logindetail">
        <Container>
          <Row>
            <Col lg={5}>
              <div className="login">
                <div className="loginimg">
                  <img src={signup} alt="" />
                  <h4>
                    Looks like you're new here! <br />
                    <p> Wishlist and Recommendations </p>
                  </h4>
                </div>
              </div>
            </Col>
            <Col lg={6}>
              <div className="logindetail">
                <h3>Create Account</h3>
                <form onSubmit={handleSend} method="post">
                  <div className="emaillogin">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M10.0002 13.9691C12.8112 13.9691 15.2699 15.2258 16.607 17.101L15.1931 17.796C14.1046 16.4555 12.1856 15.565 10.0002 15.565C7.81475 15.565 5.89571 16.4555 4.80723 17.796L3.39404 17.1002C4.73123 15.225 7.18914 13.9691 10.0002 13.9691ZM10.0002 2C12.1199 2 13.8382 3.78625 13.8382 5.9897V8.38351C13.8382 10.5193 12.2199 12.2764 10.167 12.3692L10.0002 12.3732C7.88044 12.3732 6.16207 10.587 6.16207 8.38351V5.9897C6.16207 3.85391 7.78046 2.09683 9.83336 2.004L10.0002 2ZM10.0002 3.59588C8.78063 3.59588 7.77261 4.584 7.70114 5.84939L7.69731 5.9897V8.38351C7.6961 9.67912 8.68674 10.7406 9.93258 10.7786C11.1784 10.8166 12.2273 9.8174 12.2992 8.52395L12.303 8.38351V5.9897C12.303 4.66763 11.272 3.59588 10.0002 3.59588Z"
                          fill="#1190CB"
                        />
                      </svg>
                    </span>
                    <input
                      onChange={handleChange}
                      value={user.userName}
                      onKeyUp={handleValidation}
                      type="text"
                      name="userName"
                      placeholder="Username"
                    />
                    <p className="text-danger">{userForm}</p>
                  </div>
                  <div className="emaillogin">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M2.8 3H17.2C17.6418 3 18 3.34822 18 3.77778V16.2222C18 16.6518 17.6418 17 17.2 17H2.8C2.35817 17 2 16.6518 2 16.2222V3.77778C2 3.34822 2.35817 3 2.8 3ZM16.4 6.29622L10.0576 11.8184L3.6 6.27911V15.4444H16.4V6.29622ZM4.0088 4.55556L10.0488 9.73711L16.0016 4.55556H4.0088Z"
                          fill="#1190CB"
                        />
                      </svg>
                    </span>
                    <input
                      onChange={handleChange}
                      value={user.email}
                      type="email"
                      onKeyUp={handleValidation}
                      name="email"
                      placeholder="Email address"
                    />
                    <p className="text-danger">{message}</p>
                  </div>
                  <div className="emaillogin">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M5.33333 6.66667V5.85714C5.33333 3.17462 7.42267 1 10 1C12.5773 1 14.6667 3.17462 14.6667 5.85714V6.66667H16.2222C16.6518 6.66667 17 7.0291 17 7.47619V17.1905C17 17.6376 16.6518 18 16.2222 18H3.77778C3.34822 18 3 17.6376 3 17.1905V7.47619C3 7.0291 3.34822 6.66667 3.77778 6.66667H5.33333ZM15.4444 8.28571H4.55556V16.381H15.4444V8.28571ZM9.22222 12.9259C8.61249 12.5595 8.31523 11.8125 8.49745 11.1047C8.67967 10.3969 9.29595 9.90472 10 9.90472C10.7041 9.90472 11.3203 10.3969 11.5026 11.1047C11.6848 11.8125 11.3875 12.5595 10.7778 12.9259V14.7619H9.22222V12.9259ZM6.88889 6.66667H13.1111V5.85714C13.1111 4.06879 11.7182 2.61905 10 2.61905C8.28178 2.61905 6.88889 4.06879 6.88889 5.85714V6.66667Z"
                          fill="#1190CB"
                        />
                      </svg>
                    </span>
                    <input
                      onChange={handleChange}
                      value={user.password}
                      type="password"
                      onPaste={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      onCopy={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      name="password"
                      placeholder="Password"
                      onKeyUp={validatePassword}
                    />
                    {passwordError && (
                      <p className="text-danger">{passwordError}</p>
                    )}
                  </div>
                  <div className="emaillogin">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M5.33333 6.66667V5.85714C5.33333 3.17462 7.42267 1 10 1C12.5773 1 14.6667 3.17462 14.6667 5.85714V6.66667H16.2222C16.6518 6.66667 17 7.0291 17 7.47619V17.1905C17 17.6376 16.6518 18 16.2222 18H3.77778C3.34822 18 3 17.6376 3 17.1905V7.47619C3 7.0291 3.34822 6.66667 3.77778 6.66667H5.33333ZM15.4444 8.28571H4.55556V16.381H15.4444V8.28571ZM9.22222 12.9259C8.61249 12.5595 8.31523 11.8125 8.49745 11.1047C8.67967 10.3969 9.29595 9.90472 10 9.90472C10.7041 9.90472 11.3203 10.3969 11.5026 11.1047C11.6848 11.8125 11.3875 12.5595 10.7778 12.9259V14.7619H9.22222V12.9259ZM6.88889 6.66667H13.1111V5.85714C13.1111 4.06879 11.7182 2.61905 10 2.61905C8.28178 2.61905 6.88889 4.06879 6.88889 5.85714V6.66667Z"
                          fill="#1190CB"
                        />
                      </svg>
                    </span>
                    <input
                      name="confirmPassword"
                      value={confirmPassword}
                      type="password"
                      onChange={(e) => setConfirmPassword(e.target.value)}
                      placeholder="Confirm Password"
                      onPaste={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      onCopy={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      onKeyUp={validateConfirmPassword}
                    />
                    {confirmPasswordError && (
                      <p className="text-danger">{confirmPasswordError}</p>
                    )}
                  </div>
                  <div className="remember forgoetten">
                    <Link className="remme">
                      <input type="radio" />
                      <span>Remember Me</span>
                    </Link>
                  </div>
                  <div className="btn_sub">
                    <button
                      className={`createsubmit ${
                        formIsValid ? "btn_submit" : ""
                      }`}
                      disabled={!formIsValid}
                      type="submit"
                    >
                      create account
                    </button>
                  </div>
                </form>
                <div className="account have">
                  <Link className="need">Already have an account?</Link>
                  <Link className="reginow" to="/login">
                    Login
                  </Link>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
export default Signup;
