import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import { ProUrl } from "../../helper/api";
const Admin = () => {
  const [product, setProduct] = useState({
    title: "",
    description: "",
    price: "",
    rating: "",
    category: "",
    brand: "",
  });
  const [thumbnail, setThumbnail] = useState();
  const [product_files, setProduct_files] = useState();
  const handleChange = (e) => {
    const { name, value } = e.target;
    setProduct({
      ...product,
      [name]: value,
    });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const { title, description, price, rating, category, brand } = product;
    const formData = new FormData();
    formData.append("thumbnail", thumbnail);
    formData.append("product_files", product_files);
    formData.append("data", JSON.stringify(product));

    if (title && description && price && rating && category && thumbnail && brand) {
      ProUrl.post("/add", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          // authorization: "your token comes here",
        },
      })
        .then((response) => {
          console.log(response.data);

          toast.success("Successfully addproduct !", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      console.log("Empty Filed is Not Required!");
      toast.error("Empty Filed is Not Required!", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
  };
  return (
    <>
      <section className="admin_form">
        <Container>
          <div className="admin_form_inner">
            <form onSubmit={handleSubmit}>
              <div className="emaillogin">
                <label htmlFor="title">Title</label>
                <input
                  type="text"
                  name="title"
                  value={product.title}
                  onChange={handleChange}
                  placeholder="Enter Title"
                />
              </div>
              <div className="emaillogin">
                <label htmlFor="description">Description</label>
                <input
                  type="text"
                  value={product.description}
                  onChange={handleChange}
                  name="description"
                  placeholder="Enter Description"
                />
              </div>
              <div className="emaillogin">
                <label htmlFor="price">Price</label>
                <input
                  value={product.price}
                  onChange={handleChange}
                  type="number"
                  name="price"
                  placeholder="Enter Price"
                />
              </div>
              <div className="emaillogin">
                <label htmlFor="rating">Rating</label>
                <input
                  type="number"
                  value={product.rating}
                  onChange={handleChange}
                  name="rating"
                  placeholder="Enter Rating"
                />
              </div>
              <div className="emaillogin">
                <label htmlFor="category">Category</label>
                {/* <input
                  type="text"
                  value={product.category}
                  onChange={handleChange}
                  name="category"
                  placeholder="Enter Category"
                /> */}

                <select value={product.category}
                  onChange={handleChange}
                  name="category">
                  <option value="Men’s Clothes">Men’s Clothes</option>
                  <option value="Kids’ Clothes">Kids’ Clothes</option>
                  <option value="Women’s Clothes">Women’s Clothes</option>
                  <option value="Accessories">Accessories</option>
                  <option value="Essentials">Essentials</option>
                  <option value="Fall">Fall</option>
                  <option value="Winter">Winter</option>
                  <option value="Spring">Spring</option>
                  <option value="Summer">Summer</option>
                  <option value="Beauty and Health">Beauty and Health</option>
                  <option value="Home Decor">Home Decor</option>
                  <option value="Clothes">Clothes</option>
                  <option value="Shoes">Shoes</option>
                  <option value="Underwear">Underwear</option>
                  <option value="Miscellaneous">Miscellaneous</option>

                </select>
              </div>
              <div className="emaillogin">
                <label htmlFor="thumbnail">Image</label>
                <input
                  onChange={(e) => setProduct_files(e.target.files[0])}
                  type="file"
                  name="thumbnail"
                />
              </div>

              <div className="emaillogin">
                <label htmlFor="brand">Brand</label>
                <input
                  onChange={handleChange}
                  type="text"
                  name="brand"
                />
              </div>

              <div className="emaillogin">
                <label htmlFor="thumbnail">Image</label>
                <input
                  onChange={(e) => setThumbnail(e.target.files[0])}
                  type="file"
                  name="thumbnail"
                />
              </div>
              <div className="emaillogin">
                <label htmlFor="thumbnail">Image</label>
                <input
                  onChange={(e) => setProduct_files(e.target.files[0])}
                  type="file"
                  name="thumbnail"
                />
              </div>
              <Link onClick={handleSubmit}>Add Product</Link>
            </form>
          </div>
          <ToastContainer />
        </Container>
      </section>
    </>
  );
};
export default Admin;
