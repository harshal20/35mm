import React from 'react'

const Title = ({ title_main }) => {
    return (
        <>
            <div className="feture_title">
                <h3 className='feture_title_inner'>{title_main}</h3>
            </div>
        </>
    )
}

export default Title;
