import React from "react";
import { Link } from "react-router-dom";

export const Links = ({ buttons, link }) => {
  return (
    <>
      <div className="contact_us_submit">
        <Link className="contact_submit_btn" to={link}>
          {buttons}
        </Link>
      </div>
    </>
  );
};

export const ViewCart = ({ viewsub, link }) => {
  return (
    <>
      <div className="viewbtn">
        <Link className="viewbtnbtn" to={link}>
          {viewsub}
        </Link>
      </div>
    </>
  );
};

export const BtnSubmit = ({ buttonVal, disabled }) => {
  return (
    <>
      <div className="btn_sub">
        <button className="btn_submit" disabled={disabled} type="submit">
          {buttonVal}
        </button>
      </div>
    </>
  );
};
