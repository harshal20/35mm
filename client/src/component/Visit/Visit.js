import React from "react";
import "./Visit.css";
import proimg from "../../assets/images/product/purchasehistory/productimg1.png";
import proimg2 from "../../assets/images/product/purchasehistory/productimg2.png";
import { Link } from "react-router-dom";
import { style } from "@mui/system";
const Visit = () => {
  return (
    <>
      <div className="visit">
        <div className="viditlist">
          <div className="visitboyes">
            <img src={proimg} alt="" />
          </div>
          <div className="BoysKurta">
            <h5 className="BoysKurtatext">Boys' Ethnic Kurta</h5>
            <p>
              <span style={{ color: "#1190CB" }} className="">
                $300.00
              </span>
            </p>
          </div>
          <div className="visitbtn">
            <Link to="" className="visitbtnbtn">
              <span>
                <svg
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M9.66406 9.16699V4.16699H11.3307V9.16699H16.3307V10.8337H11.3307V15.8337H9.66406V10.8337H4.66406V9.16699H9.66406Z"
                    fill="#09121F"
                  />
                </svg>
              </span>
              Add to cart
            </Link>
          </div>
        </div>
        <div className="viditlist viditlist1 ">
          <div className="visitboyes">
            <img src={proimg} alt="" />
          </div>
          <div className="BoysKurta">
            <h5 className="BoysKurtatext">Boys' Ethnic Kurta</h5>
            <p>
              <span style={{ color: "#1190CB" }} className="">
                $300.00
              </span>
            </p>
          </div>
          <div className="visitbtn">
            <Link to="" className="visitbtnbtn">
              <span>
                <svg
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M9.66406 9.16699V4.16699H11.3307V9.16699H16.3307V10.8337H11.3307V15.8337H9.66406V10.8337H4.66406V9.16699H9.66406Z"
                    fill="#09121F"
                  />
                </svg>
              </span>
              Add to cart
            </Link>
          </div>
        </div>
        <div className="viewbtn">
          <button className="viewbtnbtn">View Cart</button>
        </div>
      </div>
    </>
  );
};

export default Visit;
