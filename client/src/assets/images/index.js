import blogs_main from './blog/blogs_main.png';
import blogs_profile from './blog/blogs_profile.png'
import recent1 from './blog/recent1.png'
import recent2 from './blog/recent2.png'
import recent3 from './blog/recent3.png'
import mm_contact from './contact/mm_contact.png'
import about_main from './about/about_main.png'
import contact_main from './contact/contact_main.png'
import headerlogo from './logo/headerlogo.png'
import profileimg from './profile/profile.png'
import footerlogo from './logo/footerlogo.png'
import visa from './svg/visa.svg'
import paypal from './svg/paypal.svg'
import mastercard from './svg/mastercard.svg'
import discover from './svg/discover.svg'
import Banner_1 from "./hero/Banner_1.png"
import Banner_2 from "./hero/Banner_2.png"
import Banner_3 from "./hero/Banner_3.png"
import Banner_6 from "./hero/Banner_6.png"
import Banner_7 from "./hero/Banner_7.png"
import mmlogo from './logo/mmlogo.png'
import background from './hero/background.png'
import product1 from './product/clothes/product1.png'
import product2 from './product/clothes/product2.png'
import product3 from './product/clothes/product3.png'
import product4 from './product/clothes/product4.png'
import product5 from './product/clothes/product5.png'
import product7 from './product/clothes/product7.png'
import product8 from './product/clothes/product8.png'
import product9 from './product/clothes/product9.png'
import product10 from './product/clothes/product10.png'
import product12 from './product/clothes/product12.png'
import follow1 from './hero/follow/follow1.png'
import follow2 from './hero/follow/follow2.png'
import follow3 from './hero/follow/follow3.png'
import follow4 from './hero/follow/follow4.png'
import follow5 from './hero/follow/follow5.png'
import radioimg from './hero/radioimg.png'
import heroafter from './hero/heroafter.png'
import livestreamimg1 from './live_stream/livestreamimg1.png'
import livestreamimg2 from './live_stream/livestreamimg2.png'
import livestreamimg3 from './live_stream/livestreamimg3.png'
import livestreamimg4 from './live_stream/livestreamimg4.png'
import jeket1 from './product/jeket/jeket1.png'
import perse from "./product/perse/perse.png"
import endimg from "./endimg.png"
import shoes from "./product/shoes/shoes.png"
import gogles from "./product/gogles/gogles.png"
import girl1 from './product/clothes/girl1.png'
import girl2 from './product/clothes/girl2.png'
import girl3 from './product/clothes/girl3.png'
import girl4 from './product/clothes/girl4.png'
import cart1 from './cart/cart1.png'
import cart2 from './cart/cart2.png'
import blog1 from './blog/blog1.png'
import blog2 from './blog/blog2.png'
import blog3 from './blog/blog3.png'
import blog4 from './blog/blog4.png'
import blog5 from './blog/blog5.png'
import blog6 from './blog/blog6.png'
import live from './live_stream/live.png'


export {
    // logo
    footerlogo,
    headerlogo,
    mmlogo,
    // svg
    paypal,
    mastercard,
    discover,
    visa,
    // blog
    blogs_main,
    blogs_profile,
    recent1,
    recent2,
    recent3,
    blog1,
    blog2,
    blog3,
    blog4,
    blog5,
    blog6,
    // about
    about_main,
    // contact
    mm_contact,
    contact_main,
    // hero
    Banner_1,
    Banner_2,
    Banner_3,
    Banner_6,
    Banner_7,
    background,
    radioimg,
    heroafter,
    follow1,
    follow2,
    follow3,
    follow4,
    follow5,
    // product
    perse,
    endimg,
    shoes,
    gogles,
    product1,
    product2,
    product3,
    product4,
    jeket1,
    girl1,
    girl2,
    girl3,
    girl4,
    product5,
    product7,
    product8,
    product9,
    product10,
    product12,
    // live_strimg
    livestreamimg1,
    livestreamimg2,
    livestreamimg3,
    livestreamimg4,
    live,
    // cart
    cart1,
    cart2,
    // 
    // profile
    profileimg,
}