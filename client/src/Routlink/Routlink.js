import React from "react";
// all css
import "../Pages/allCss/index";
import "bootstrap/dist/css/bootstrap.min.css";
import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import Dashboard from "../Dashboard/dashboard/Dashboard";
import Pricing from "../Pages/pricing/Pricing";
import Blog from "../Pages/blog/Blog";
import Blogs from "../Pages/blog/Blogs";
import OurPolicies from "../Pages/our_policies/OurPolicies";
import Heropage from "../Pages/Heropage/Heropage";
import Shopingcart from "../Pages/Shopingcart/Shopingcart/Shopingcart";
import Bilingdetail from "../Pages/Shopingcart/Bilingdetail/Bilingdetail";
import Contact from "../Pages/contact/Contact";
import PurchaseHistory from "../Dashboard/PurchaseHistory/PurchaseHistory";
import Wishlist from "../Dashboard/Wishlist/Wishlist";
import Conversations from "../Dashboard/Conversations/Conversations";
import ContactUs from "../Pages/contact/ContactUs";
import AboutUs from "../Pages/aboutUs/AboutUs";
import LiveStreaming from "../Pages/liveStreaming/LiveStreaming";
import Produtviewpage from "../component/Productview/Produtviewpage";
import ProductFilter from "../component/Productfilter/Productfilter";
import Login from "../component/Auth/Login/Login";
import Signup from "../component/Auth/SignUP/Signup";
import { Header, Footer, Admin, Logic } from "../component";
import { ToastContainer } from "react-toastify";
import Cookies from "js-cookie";
import { OrderTracking } from "../Pages";
import Downloads from "../Dashboard/Downloads/Downloads";

import ProductImagesSlider from "../component/Productview/productslider/ProductImagesSlider";
import Productslidernew from './../component/Productview/Productslidernew/Productslidernew';

const Routlink = () => {
  let location = useLocation();
  const getCookies = Cookies.get("token");

  return (
    <div className="App">
      {location.pathname === "/login" ||
        location.pathname === "/Signup" ||
        location.pathname === "/admin" ? (
        ""
      ) : (
        <Header />
      )}

      {/* <RouteToTop> */}
      <Routes>
        <Route exact path="/" element={<Heropage />} />
        <Route exact path="/Dashboard" element={<Dashboard />} />
        <Route exact path="/PurchaseHistory" element={<PurchaseHistory />} />
        <Route exact path="/Wishlist" element={<Wishlist />} />
        <Route exact path="/Conversations" element={<Conversations />} />
        <Route exact path="/Shopingcart" element={<Shopingcart />} />
        <Route exact path="/Bilingdetail" element={<Bilingdetail />} />
        <Route exact path="/Produtviewpage/:id" element={<Produtviewpage />} />
        <Route exact path="/ProductFilter" element={<ProductFilter />} />
        <Route exact path="/pricing" element={<Pricing />} />
        <Route exact path="/blog" element={<Blog />} />
        <Route exact path="/blogs" element={<Blogs />} />
        <Route exact path="/policies" element={<OurPolicies />} />
        <Route exact path="/contact" element={<Contact />} />
        <Route exact path="/contactUs" element={<ContactUs />} />
        <Route exact path="/aboutUs" element={<AboutUs />} />
        <Route exact path="/liveStreaming" element={<LiveStreaming />} />
        <Route exact path="/orderTracking" element={<OrderTracking />} />
        <Route exact path="/downloads" element={<Downloads />} />
        <Route exact path="/ProductImagesSlider" element={<ProductImagesSlider />} />
        <Route exact path="/Productslidernew" element={<Productslidernew />} />
        <Route
          exact
          path="/login"
          element={!getCookies ? <Login /> : <Navigate to={"/"} replace />}
        />
        <Route exact path="/Signup" element={<Signup />} />
        <Route exact path="/admin" element={<Admin />} />
        <Route exact path="*" element={<Navigate to="/" replace />} />
      </Routes>
      {/* </RouteToTop> */}

      {location.pathname === "/login" ||
        location.pathname === "/Signup" ||
        location.pathname === "/admin" ? (
        ""
      ) : (
        <Footer />
      )}

      <ToastContainer />
      <Logic />
    </div>
  );
};

export default Routlink;
