import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { heroafter, radioimg } from '../../../assets/images'
import "./Hero.css"



const Hero = () => {

    return (
        <>
            <section className='herosec' >
                <Container>
                    <Row>
                        <Col lg={5}>
                            <div className="good">
                                <h4 data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
                                    <span>Good Prise</span><p>Sale offer 10% off</p>
                                </h4>
                                <h2 data-aos="fade-up" data-aos-delay="400" data-aos-duration="1200">Saregama<br />
                                    Carvaan</h2>
                                <Link data-aos="fade-up" data-aos-delay="500" data-aos-duration="1400"> Shop Now <svg id="arrow-right-line_1_" data-name="arrow-right-line (1)" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                    <path id="Path_17" data-name="Path 17" d="M0,0H20V20H0Z" fill="none" />
                                    <path id="Path_18" data-name="Path 18" d="M13.129,9.257,9.106,5.272l1.06-1.05L16,10l-5.833,5.778-1.06-1.05,4.023-3.985H4V9.257Z" fill="#fff" />
                                </svg>
                                </Link>
                            </div>
                        </Col>
                        <Col lg={7}>
                            <div className="images">
                                <div className="heroimg">
                                    <img data-aos="zoom-in" data-aos-duration="1200" src={radioimg} alt="" className='magniflier' />
                                </div>
                                <span><img src={heroafter} alt="" /></span>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Hero