import React from 'react'
import { Container, Row } from 'react-bootstrap';
import { follow1, follow2, follow3, follow4, follow5 } from '../../../assets/images';
import Title from '../../../component/layout/Title';
import "./Follows.css"

const Follows = () => {
    return (
        <>
            <section className='Follows'>
                <Container>
                    <Row>
                        <div className="followdetail">
                            <Title title_main={"Follow Us on Instagram"} />
                            <ul className='foloowimg'>
                                <li data-aos-duration="1400" data-aos="zoom-in-up"><img src={follow1} alt="" /></li>
                                <li data-aos-duration="1400" data-aos="zoom-in-up"><img src={follow2} alt="" /></li>
                                <li data-aos-duration="1400" data-aos="zoom-in-up"><img src={follow3} alt="" /></li>
                                <li data-aos-duration="1400" data-aos="zoom-in-up"><img src={follow4} alt="" /></li>
                                <li data-aos-duration="1400" data-aos="zoom-in-up"><img src={follow5} alt="" /></li>
                            </ul>
                        </div>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Follows