import React from 'react'
import { Link } from 'react-router-dom';
import { Container, Row } from 'react-bootstrap';
import "./Bestseller.css"
import { mmlogo } from '../../../assets/images';
import Title from '../../../component/layout/Title';

const Bestseller = () => {
    return (
        <>
            <section className='Bestseller'>
                <Container>
                    <Row>
                        <div className="bestsell">
                            <Title title_main={"Best Sellers"} />
                            <ul className='shopping'>
                                <li>
                                    <div className="shopers">
                                        <img src={mmlogo} alt="" />
                                        <div className="shopsssss">
                                            <h4>Your Shop</h4>
                                            <div className="ratt">
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="black" />
                                                </svg>
                                                <span>5.0</span>
                                            </div>
                                            <Link to="">View Store</Link>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="shopers">
                                        <img src={mmlogo} alt="" />
                                        <div className="shopsssss">
                                            <h4>Your Shop</h4>
                                            <div className="ratt">
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="black" />
                                                </svg>
                                                <span>5.0</span>
                                            </div>
                                            <Link to="">View Store</Link>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="shopers">
                                        <img src={mmlogo} alt="" />
                                        <div className="shopsssss">
                                            <h4>Your Shop</h4>
                                            <div className="ratt">
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="black" />
                                                </svg>
                                                <span>5.0</span>
                                            </div>
                                            <Link to="">View Store</Link>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="shopers">
                                        <img src={mmlogo} alt="" />
                                        <div className="shopsssss">
                                            <h4>Your Shop</h4>
                                            <div className="ratt">
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="black" />
                                                </svg>
                                                <span>5.0</span>
                                            </div>
                                            <Link to="">View Store</Link>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="shopers">
                                        <img src={mmlogo} alt="" />
                                        <div className="shopsssss">
                                            <h4>Your Shop</h4>
                                            <div className="ratt">
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="black" />
                                                </svg>
                                                <span>5.0</span>
                                            </div>
                                            <Link to="">View Store</Link>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="shopers">
                                        <img src={mmlogo} alt="" />
                                        <div className="shopsssss">
                                            <h4>Your Shop</h4>
                                            <div className="ratt">
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="#FF7D51" />
                                                </svg>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M7.99992 11.7149L3.41883 14.208L4.44183 9.20169L0.586914 5.73617L5.80063 5.13501L7.99992 0.5L10.1992 5.13501L15.4129 5.73617L11.558 9.20169L12.581 14.208L7.99992 11.7149Z" fill="black" />
                                                </svg>
                                                <span>5.0</span>
                                            </div>
                                            <Link to="">View Store</Link>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Bestseller