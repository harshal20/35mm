import React, { useState } from "react";
import { Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Title from "../../../component/layout/Title";
import "./Womencloth.css";

const Womencloth = () => {
  const [data, setData] = useState([]);
  console.log(data);
  React.useEffect(() => {
    fetch("http://localhost:5000/api/product/allProducts")
      .then((response) => response.json())

      .then((res) => {
        setData(res.data);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <>
      <section className="womens">
        <Container>
          <Row>
            <div className="womencloth">
              <Title title_main={"Women’s Clothing and Fashion"} />
              <ul className="productdetail">
                {data.slice(0, 4).map((value, index) => {
                  return (
                    <>
                      <li key={index}>
                        <img
                          src={`http://localhost:5000/${value.thumbnail.path}`}
                          alt=""
                        />
                        <h4 className="contact_product_head">{value.title}</h4>
                        <Link to="">
                          <svg
                            id="star-fill_5_"
                            data-name="star-fill (5)"
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                          >
                            <path
                              id="Path_19"
                              data-name="Path 19"
                              d="M0,0H16V16H0Z"
                              fill="none"
                            />
                            <path
                              id="Path_20"
                              data-name="Path 20"
                              d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z"
                              fill="#ff7d51"
                            />
                          </svg>
                          <svg
                            id="star-fill_5_"
                            data-name="star-fill (5)"
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                          >
                            <path
                              id="Path_19"
                              data-name="Path 19"
                              d="M0,0H16V16H0Z"
                              fill="none"
                            />
                            <path
                              id="Path_20"
                              data-name="Path 20"
                              d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z"
                              fill="#ff7d51"
                            />
                          </svg>
                          <svg
                            id="star-fill_5_"
                            data-name="star-fill (5)"
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                          >
                            <path
                              id="Path_19"
                              data-name="Path 19"
                              d="M0,0H16V16H0Z"
                              fill="none"
                            />
                            <path
                              id="Path_20"
                              data-name="Path 20"
                              d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z"
                              fill="#ff7d51"
                            />
                          </svg>
                          <svg
                            id="star-fill_5_"
                            data-name="star-fill (5)"
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                          >
                            <path
                              id="Path_19"
                              data-name="Path 19"
                              d="M0,0H16V16H0Z"
                              fill="none"
                            />
                            <path
                              id="Path_20"
                              data-name="Path 20"
                              d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z"
                              fill="#ff7d51"
                            />
                          </svg>
                          <svg
                            id="star-fill_5_"
                            data-name="star-fill (5)"
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                          >
                            <path
                              id="Path_19"
                              data-name="Path 19"
                              d="M0,0H16V16H0Z"
                              fill="none"
                            />
                            <path
                              id="Path_20"
                              data-name="Path 20"
                              d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z"
                              fill="#c0ccda"
                            />
                          </svg>
                        </Link>
                        <span> {value.rating} </span>
                        <p>{value.price}</p>
                      </li>
                    </>
                  );
                })}
              </ul>
            </div>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Womencloth;
