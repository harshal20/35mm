import React from 'react'
import { Container, Row } from 'react-bootstrap'
import { livestreamimg1, livestreamimg2, livestreamimg3, livestreamimg4 } from '../../../assets/images'
import Title from '../../../component/layout/Title';
import "./Livestream.css"

const Livestream = () => {
    return (
        <>
            <section className='lives'>
                <Container>
                    <Row>
                        <div className="livestreamdetail">
                            <Title title_main={"Best Sellers"} />
                            <p> Catch some of our 35mm sellers live right now </p>

                            <ul className='liveimg'>
                                <li data-aos-duration="1400" data-aos="zoom-in-up"><img src={livestreamimg1} alt="" />
                                    <h4><span></span> LIVE</h4>
                                </li>
                                <li data-aos-duration="1400" data-aos="zoom-in-up" ><img src={livestreamimg2} alt="" />
                                    <h4><span></span> LIVE</h4></li>
                                <li data-aos-duration="1400" data-aos="zoom-in-up" ><img src={livestreamimg3} alt="" />
                                    <h4> <span></span> LIVE</h4></li>
                                <li data-aos-duration="1400" data-aos="zoom-in-up" > <img src={livestreamimg4} alt="" />
                                    <h4><span></span> LIVE</h4></li>
                            </ul>
                        </div>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Livestream