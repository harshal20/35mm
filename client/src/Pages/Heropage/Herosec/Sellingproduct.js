import React from 'react'
import { Container, Row } from 'react-bootstrap'
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import "./Sellingproduct.css"
import { Link } from 'react-router-dom';
import { endimg, gogles, jeket1, perse, shoes } from '../../../assets/images';
import Title from '../../../component/layout/Title';

const Sellingproduct = () => {
    return (
        <>
            <section className='selling'>
                <Container>
                    <Row>
                        <div className="sellingproduct">
                            <Title title_main={"Best Featured Products"} />
                            <div className="productnav">
                                <Tabs
                                // defaultActiveKey="profile"
                                // id="uncontrolled-tab-example"
                                // className="mb-3"
                                >
                                    <Tab eventKey="CLOTHING" title="CLOTHING">
                                        <ul className="shoppro">

                                            <li>
                                                <img src={jeket1} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Jacket with pouch pocket</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>
                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={perse} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Backpack with metal fastener</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={shoes} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Trainers with heel detail</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={gogles} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Woman Sunglasses</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                        </ul>
                                    </Tab>
                                    <Tab eventKey="SHOES" title="SHOES">
                                        <ul className="shoppro">

                                            <li>
                                                <img src={jeket1} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Jacket with pouch pocket</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>
                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={perse} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Backpack with metal fastener</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={shoes} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Trainers with heel detail</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={gogles} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Woman Sunglasses</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                        </ul>
                                    </Tab>
                                    <Tab eventKey="HANDBAGS" title="HANDBAGS" >
                                        <ul className="shoppro">

                                            <li>
                                                <img src={jeket1} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Jacket with pouch pocket</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>
                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={perse} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Backpack with metal fastener</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={shoes} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Trainers with heel detail</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={gogles} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Woman Sunglasses</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                        </ul>
                                    </Tab>
                                    <Tab eventKey="ACCESSOIRES" title="ACCESSOIRES" >
                                        <ul className="shoppro">

                                            <li>
                                                <img src={jeket1} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Jacket with pouch pocket</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>
                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={perse} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Backpack with metal fastener</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={shoes} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Trainers with heel detail</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={gogles} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Woman Sunglasses</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                        </ul>
                                    </Tab><Tab eventKey="FASHION" title="FASHION" >
                                        <ul className="shoppro">

                                            <li>
                                                <img src={jeket1} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Jacket with pouch pocket</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>
                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={perse} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Backpack with metal fastener</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={shoes} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Trainers with heel detail</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={gogles} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Woman Sunglasses</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                        </ul>
                                    </Tab><Tab eventKey="OTHER" title="OTHER" >
                                        <ul className="shoppro">

                                            <li>
                                                <img src={jeket1} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Jacket with pouch pocket</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>
                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={perse} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Backpack with metal fastener</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={shoes} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Trainers with heel detail</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                            <li>
                                                <img src={gogles} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                <h4>Woman Sunglasses</h4>
                                                <div className="rating">
                                                    <div className="rate">
                                                        <Link to="">
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#ff7d51" />
                                                            </svg>
                                                            <svg id="star-fill_5_" data-name="star-fill (5)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                <path id="Path_19" data-name="Path 19" d="M0,0H16V16H0Z" fill="none" />
                                                                <path id="Path_20" data-name="Path 20" d="M8,11.715,3.419,14.208,4.442,9.2.587,5.736l5.214-.6L8,.5l2.2,4.635,5.214.6L11.558,9.2l1.023,5.006Z" fill="#c0ccda" />
                                                            </svg>

                                                        </Link>
                                                        <span>5.0</span>
                                                    </div>
                                                    <div className="last">
                                                        <img src={endimg} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                                                    </div>
                                                </div>

                                                <p>$300.00</p>
                                            </li>
                                        </ul>
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Sellingproduct