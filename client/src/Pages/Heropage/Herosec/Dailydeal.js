import React from 'react'
import { Link } from 'react-router-dom';
import { background } from '../../../assets/images';
import "./Dailydeal.css"

const Dailydeal = () => {
    return (
        <>
            <section className='dailydealdetail'>
                <img src={background} alt="" />
                <div className="hurryup">
                    <h3 data-aos="fade-up"
                        data-aos-duration="1400">35mmerific Deals!</h3>
                    <h4 data-aos="fade-up"
                        data-aos-duration="1600"><span> Hurry Up!</span> Daily Deal Of The Day</h4>
                    <p data-aos="fade-up"
                        data-aos-duration="1700">We update new deals daily from our awesome sellers. Don’t miss out on some of their<br /> most amazing products on sale. We only select the best bang for your buck!</p>
                    <Link to="/">Discover Now</Link>
                </div>
            </section>
        </>
    )
}

export default Dailydeal