import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Banner_6, Banner_7 } from '../../../assets/images'
import "./Skinbg.css"

const Skinbg = () => {
    return (
        <>
            <section className='skin'>
                <Container>
                    <Row>
                        <Col lg={6}>
                            <div className="skin">
                                <img src={Banner_6} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                            </div>
                        </Col>
                        <Col lg={6}>
                            <div className="skin">
                                <img src={Banner_7} alt="" data-aos-duration="1400" data-aos="zoom-in-up" />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Skinbg