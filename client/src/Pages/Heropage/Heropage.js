import Hero from "./Herosec/Hero";
import Fashion from "./Fashion/Fashion";
import React from 'react'
import Featured from "./Herosec/Featured";
import Dailydeal from "./Herosec/Dailydeal";
import Sellingproduct from "./Herosec/Sellingproduct";
import Skinbg from "./Herosec/Skinbg";
import Womencloth from "./Herosec/Womencloth";
import Livestream from "./Herosec/Livestream";
import Bestseller from "./Herosec/Bestseller";
import Follows from "./Herosec/Follows";

const Heropage = () => {
    return (
        <>
            <Hero />
            <Fashion />
            <Featured />
            <Dailydeal />
            <Sellingproduct />
            <Skinbg />
            <Womencloth />
            <Livestream />
            <Bestseller />
            <Follows />
        </>
    )
}

export default Heropage;