import React from 'react'
import { Container, Row } from 'react-bootstrap'
import { Banner_1, Banner_2, Banner_3 } from '../../../assets/images'
import "../Herosec/Fashion.css"

const Fashion = () => {
    return (
        <section className='fahion'>
            <Container>
                <Row>
                    <div className="fashiondetail">
                        <ul className='fashionbanner'>
                            <li>
                                <img data-aos-duration="1400" data-aos="zoom-in-up" src={Banner_1} alt="" />
                                <img data-aos-duration="1500" data-aos="zoom-in-up" src={Banner_2} alt="" />
                                <img data-aos-duration="1600" data-aos="zoom-in-up" src={Banner_3} alt="" />
                            </li>
                        </ul>
                    </div>
                </Row>
            </Container>
        </section>
    )
}

export default Fashion