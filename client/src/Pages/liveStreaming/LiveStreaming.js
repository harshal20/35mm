import { Rating } from "@mui/material";
import React from "react";
import { Breadcrumb, Col, Container, Row } from "react-bootstrap";
import {
  live,
  livestreamimg1,
  livestreamimg2,
  livestreamimg3,
  livestreamimg4,
} from "../../assets/images";
import { Link } from "react-router-dom";
import RelatedProduct from "../../component/relatedProduct/RelatedProduct";
import { Links } from "../../component/layout/Buttons";
import Title from "../../component/layout/Title";

const LiveStreaming = () => {
  return (
    <>
      <section className="live_streaming">
        <div className="breadcrumb_pri">
          <div className="breadcrumb_pri_inner">
            <Breadcrumb>
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item active>Live Streaming</Breadcrumb.Item>
            </Breadcrumb>
          </div>
        </div>
        <Container>
          <div className="live_streaming_main">
            <Row>
              <Col lg={4}>
                <div className="live_streaming_inner">
                  <h6 className="pricing_head">Flared Trousers</h6>
                  <div className="live_streaming_about">
                    <span className="live_streaming_bout">About</span>
                    <p className="live_streaming_para">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aenean id interdum felis. Nunc eu ante id dui suscipit
                      tempor eu ut ligula. Sed quis dictum enim. Curabitur
                      ornare nisl non ante varius aliquet. Sed dui velit,
                      posuere id cursus sit amet, hendrerit ac nulla. Ut
                      finibus, quam ut sollicitudin facilisis, velit tellus
                      blandit risus.
                    </p>
                  </div>
                  <div className="live_streaming_price">
                    <span className="live_streaming_bout">Price</span>
                    <p className="live_streaming_para">$274.00 - $309.00</p>
                  </div>
                  <div className="live_streaming_btn">
                    <Links link={"/"} buttons={"Add to Cart"} />
                  </div>
                </div>
              </Col>
              <Col lg={7}>
                <div className="live_streaming_img">
                  <img src={live} alt="" />
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </section>
      <section className="livestrimg_data">
        <Container>
          <div className="live_streaming_video">
            <Title title_main={"Live streaming"} />
            <ul className="liveimg">
              <li>
                <Link to={"/"}>
                  <div className="live_stream_inner_img">
                    <img src={livestreamimg1} alt="" />
                    <h4>
                      <span></span> LIVE
                    </h4>
                  </div>
                  <h6 className="contact_product_head">
                    Jacket with pouch pocket
                  </h6>
                  <span className="contact_product_rating">
                    <Rating
                      name="half-rating-read"
                      defaultValue={5.0}
                      precision={4}
                      readOnly
                    />
                    <span className="contact_product_point">5.0</span>
                  </span>
                </Link>
              </li>
              <li>
                <Link to={"/"}>
                  <div className="live_stream_inner_img">
                    <img src={livestreamimg2} alt="" />
                    <h4>
                      <span></span> LIVE
                    </h4>
                  </div>
                  <h6 className="contact_product_head">
                    Low waist ripped jeans
                  </h6>
                  <span className="contact_product_rating">
                    <Rating
                      name="half-rating-read"
                      defaultValue={5.0}
                      precision={4}
                      readOnly
                    />
                    <span className="contact_product_point">5.0</span>
                  </span>
                </Link>
              </li>
              <li>
                <Link to={"/"}>
                  <div className="live_stream_inner_img">
                    <img src={livestreamimg3} alt="" />
                    <h4>
                      <span></span> LIVE
                    </h4>
                  </div>
                  <h6 className="contact_product_head">Sweater with slogan</h6>
                  <span className="contact_product_rating">
                    <Rating
                      name="half-rating-read"
                      defaultValue={5.0}
                      precision={4}
                      readOnly
                    />
                    <span className="contact_product_point">5.0</span>
                  </span>
                </Link>
              </li>
              <li>
                <Link to={"/"}>
                  <div className="live_stream_inner_img">
                    <img src={livestreamimg4} alt="" />
                    <h4>
                      <span></span> LIVE
                    </h4>
                  </div>
                  <h6 className="contact_product_head">Flared trousers</h6>
                  <span className="contact_product_rating">
                    <Rating
                      name="half-rating-read"
                      defaultValue={5.0}
                      precision={4}
                      readOnly
                    />
                    <span className="contact_product_point">5.0</span>
                  </span>
                </Link>
              </li>
            </ul>
          </div>
        </Container>
      </section>
      <section
        className="live_streaming_releted"
        style={{ paddingBottom: "60px" }}
      >
        <Container>
          <Title title_main={"Related Products"} />
          <RelatedProduct />
        </Container>
      </section>
    </>
  );
};

export default LiveStreaming;
