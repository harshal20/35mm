import React, { useState } from "react";
import { Container, Table } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import { product1 } from "../../assets/images";
import { Links } from "../../component/layout/Buttons";
import { Link } from "react-router-dom";

const Compare = () => {
  const [show, setShow] = useState(true);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <section className="compare">
        <Container>
          <button onClick={handleShow}>Compare</button>

          <Modal
            className="compre_model"
            centered
            show={show}
            onHide={handleClose}
          >
            <Modal.Header className="compare_model_head" closeButton>
              <Modal.Title className="compare_model_title">
                <div className="comare_head">
                  <h6 className="pricing_head">Compare</h6>
                  <Link to={"/"} className="remove_all_btn">
                    <span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M16.6657 5.83408V17.4992C16.6657 17.9594 16.2927 18.3324 15.8325 18.3324H4.16739C3.70722 18.3324 3.33417 17.9594 3.33417 17.4992V5.83408H1.66772V4.16764H18.3322V5.83408H16.6657ZM5.00062 5.83408V16.666H14.9993V5.83408H5.00062ZM5.83384 1.66797H14.1661V3.33441H5.83384V1.66797ZM9.16673 8.33375H10.8332V14.1663H9.16673V8.33375Z"
                          fill="#FF5151"
                        />
                      </svg>
                    </span>
                    <p>Remove All</p>
                  </Link>
                </div>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body className="compare_model_body">
              <div className="table_main">
                <Table bordered className="compare_table_main">
                  <tbody className="compare_table_tbody">
                    <tr>
                      <th>PRODUCTS</th>
                      <td>
                        <div className="compare_table_data_inner">
                          <Link to={"/"} className="compare_remove_inner">
                            Remove
                          </Link>
                          <img src={product1} alt="img" />
                          <h6>Flared Trousers</h6>
                          <p>$274.00 - $309.00</p>
                          <Links link={"/"} buttons={"Add to cart"} />
                        </div>
                      </td>
                      <td>
                        <div className="compare_table_data_inner">
                          <Link to={"/"} className="compare_remove_inner">
                            Remove
                          </Link>
                          <img src={product1} alt="img" />
                          <h6>Flared Trousers</h6>
                          <p>$274.00 - $309.00</p>
                          <Links link={"/"} buttons={"Add to cart"} />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th>Description</th>
                      <td>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Nunc ullamcorper dolor quis purus lacinia, nec
                          ultricies mauris venenatis. Suspendisse mattis augue
                          quis tincidunt interdum. Fusce eget varius ante. Nulla
                          sapien dolor, elementum at scelerisque eu, aliquet
                          vitae libero.
                        </p>
                      </td>
                      <td>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Nunc ullamcorper dolor quis purus lacinia, nec
                          ultricies mauris venenatis. Suspendisse mattis.
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <th>Meta</th>
                      <td>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Nunc ullamcorper dolor.
                        </p>
                      </td>
                      <td>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Nunc ullamcorper dolor.
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <th>Availability</th>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                    </tr>
                    <tr>
                      <th>SKU</th>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                    </tr>
                    <tr>
                      <th>Color</th>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                    </tr>
                    <tr>
                      <th>Size</th>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                      <td>
                        <div className="compare_table_data_inner">In Stock</div>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Modal.Body>
          </Modal>
        </Container>
      </section>
    </>
  );
};

export default Compare;
