import React from "react";
import { Breadcrumb, Container } from "react-bootstrap";
import { Links } from "../../component/layout/Buttons";
import cartpopimg1 from "../../assets/images/cartpopimg1.png";

const OrderTracking = () => {
  return (
    <div>
      <section className="order_tracking">
        <div className="breadcrumb_pri">
          <div className="breadcrumb_pri_inner">
            <Breadcrumb>
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item>My Account</Breadcrumb.Item>
              <Breadcrumb.Item>Purchase History</Breadcrumb.Item>
              <Breadcrumb.Item active>OD12366325220</Breadcrumb.Item>
            </Breadcrumb>
          </div>
        </div>
        <Container>
          <div className="order_tracking_main">
            <div className="order_tracking_inner">
              <div className="order_id">
                <h6 className="pricing_head">Order Tracking</h6>
                <span className="order_id_num">
                  Order ID:
                  <p className="live_streaming_para">OD12366325220</p>
                </span>
              </div>
              <div className="order_track">
                <div className="order_item_detail">
                  <div className="order_trake_img">
                    <img src={cartpopimg1} alt="" />
                  </div>
                  <div className="order_trake_details">
                    <h5>Jacket with pouch pocket</h5>
                    <div className="order_color">
                      <span className="order_color_main">Color:</span>
                      <span className="order_name">Black-White</span>
                    </div>
                    <div className="order_color">
                      <span className="order_color_main">Size:</span>
                      <span className="order_name">S</span>
                    </div>
                    <p className="order_price">$130.00</p>
                  </div>
                </div>
                <div className="order_delivered">
                  <p className="live_streaming_para">
                    You item has been delivered
                  </p>
                  <ul className="order_status">
                    <li className="order_track_process">
                      <p className="order_trake_type">Ordered</p>
                      <span className="order_color_main">Tue, 24th Jan</span>
                    </li>
                    <li className="order_track_process">
                      <p className="order_trake_type">Picked</p>
                      <span className="order_color_main">Wed, 25th Jan</span>
                    </li>
                    <li className="order_track_process">
                      <p className="order_trake_type">Shipped</p>
                      <span className="order_color_main">Thu, 27th Jan</span>
                    </li>
                    <li className="order_track_process">
                      <p className="order_trake_type">Delivered</p>
                      <span className="order_color_main">Sun, 29th Jan</span>
                    </li>
                  </ul>
                </div>
                <div className="order_deliver_date">
                  <div className="order_deliverd_date">
                    <div className="order_delivered_para">
                      <span className="order_box"></span>
                      <p className="live_streaming_para">
                        Will Deliver on 29th Aug
                      </p>
                    </div>
                    <span className="order_color_main">
                      Your item will be deliver on 29th Aug, 2020
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="order_trake_address">
              <div className="order_price">Delivery Address</div>
              <ul className="delivery_address">
                <li className="delivery_name_address">
                  <div className="delivery_owenr_name">
                    <p className="martin">Martin Willem</p>
                    <span>Home</span>
                  </div>
                  <p className="order_color_main">
                    A-432, Apple Square, 24th East Street, New York, 960012
                  </p>
                </li>
                <li className="delivery_name_address">
                  <p className="martin">Contact Details</p>
                  <p className="order_color_main">
                    +64 987 654 3210 Martinwiller@gmail.com
                  </p>
                </li>
                <li className="delivery_name_address">
                  <div className="order_invoice">
                    <p className="">Invoice</p>
                    <p className="order_color_main">Download your Invoice</p>
                  </div>
                  <div className="">
                    <Links link={"/"} buttons={"Download"} />
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </Container>
      </section>
    </div>
  );
};

export default OrderTracking;
