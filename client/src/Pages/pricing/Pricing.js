import React from 'react'
import { Breadcrumb, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Pricing = () => {
    return (
        <>
            <section className='pricing'>
                <div className="breadcrumb_pri">
                    <div className="breadcrumb_pri_inner">
                        <Breadcrumb>
                            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                            <Breadcrumb.Item active>Pricing</Breadcrumb.Item>
                        </Breadcrumb>
                    </div>
                </div>
                <Container>
                    <div className="pricing_main">
                        <div>
                            <h2 className='pricing_head'>Pricing</h2>
                            <Link className='pricing_wave' to={'/'}>Join the new wave.</Link>
                            <p className='pricing_para'>Start selling on 35mm today by starting with our free  <span className='trial'> 14 day trial!</span></p>
                        </div>
                        <div className="pricing_cars">
                            <div className="pricing_inner">
                                <div className="pricing_image pricing_image1">
                                    <p className='pricing_pro'>5 products</p>
                                    <p className='pricing-price'>Free</p>
                                    <p className='pricing-fee'>2.5% transaction fee</p>
                                    <div className="sign_up_btn">
                                        <Link to={"/"}>Sign up free</Link>
                                    </div>
                                </div>
                                <ul className='pricing-details'>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>3 images per product</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Augmented reality tools</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Live streaming tools</p>
                                    </li>
                                </ul>
                            </div>
                            <div className="pricing_inner">
                                <div className="pricing_image pricing_image2">
                                    <p className='pricing_pro'>15 products</p>
                                    <p className='pricing-price'>$10.99</p>
                                    <p className='pricing-fee'>2.5% transaction fee</p>
                                    <div className="sign_up_btn">
                                        <Link to={"/"}>Sign up free</Link>
                                    </div>
                                </div>
                                <ul className='pricing-details'>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>5 images per product</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Inventory tracking</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Analytics</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Abandoned cart</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Augmented reality tools</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Live streaming tools</p>
                                    </li>
                                </ul>
                            </div>
                            <div className="pricing_inner">
                                <div className="pricing_image pricing_image3">
                                    <p className='pricing_pro'>20 products</p>
                                    <p className='pricing-price'>$15.99</p>
                                    <p className='pricing-fee'>2.5% transaction fee</p>
                                    <div className="sign_up_btn">
                                        <Link to={"/"}>Sign up free</Link>
                                    </div>
                                </div>
                                <ul className='pricing-details'>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>5 images per product</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Inventory tracking</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Analytics</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Abandoned cart</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Augmented reality tools</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Live streaming tools</p>
                                    </li>
                                </ul>
                            </div>
                            <div className="pricing_inner">
                                <div className="pricing_image pricing_image4">
                                    <p className='pricing_pro'>25 products</p>
                                    <p className='pricing-price'>$20.99</p>
                                    <p className='pricing-fee'>2.5% transaction fee</p>
                                    <div className="sign_up_btn">
                                        <Link to={"/"}>Sign up free</Link>
                                    </div>
                                </div>
                                <ul className='pricing-details'>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>5 images per product</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Inventory tracking</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Analytics</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Abandoned cart</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Featured product listings</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Augmented reaality tools</p>
                                    </li>
                                    <li className='pricing-li'>
                                        <span className="pricing-tools">
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0C10.2301 2.99791 8.26 5.84897 6.252 9.58591C5.01791 7.97888 4.02497 6.92991 3 6H0C2.34209 8.74 3.60209 10.841 6 15H8C9.92 9.76497 11.6701 6.07791 16 0H13Z" fill="#1190CB" />
                                            </svg>
                                        </span>
                                        <p>Live streaming tools </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="pricing_data">
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>Social Commerce is Accelerating, and 35mm Puts Sellers in the Driver’s Seat</span>
                                <p className='pricing_data_para'>“Small sellers can’t compete in the eCommerce space anymore.” As a seller, we understand how real the statement above probably feels. And who can blame you? With major retailers paying billions of dollars each year for top ad spots on major platforms, the competition is stiff. Coupled with ineffective algorithms on major eCommerce sites that hinder your sales, buyers simply ‘aren’t buying it’ anymore.</p>
                            </div>
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>That’s why 35mm is different.</span>
                                <p className='pricing_data_para'>By leveraging the power of video commerce, augmented reality (AR), community connection, and exclusivity, 35mm puts sellers at the wheel.</p>
                            </div>
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>But how does it work?</span>
                                <p className='pricing_data_para'>35mm is an interactive eCommerce platform that helps sellers get discovered. Buyers are seeking authenticity from brands that they identify with. In turn, buyers turn into repeat buyers after discovering products they love from brands they feel good about supporting.</p>
                            </div>
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>So where do video and AR come into play?</span>
                                <p className='pricing_data_para'>As we speak, social commerce is taking Asian markets by storm as a $2 trillion dollar industry. Video commerce is at the forefront of this shift.
                                </p>
                            </div>
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>Here’s why:</span>
                                <ul className="pricing_here">
                                    <li className='pricing_li'>Video tells a story where product images alone fall flat</li>
                                    <li className='pricing_li'>Products gain newfound visibility & exposure through video content</li>
                                    <li className='pricing_li'>Customers report higher rates of interaction with buyers who incorporate video</li>
                                    <li className='pricing_li'>Video in the form of live streaming helps brands get noticed by new patrons</li>
                                </ul>
                                <p className='pricing_data_para'>Further expanding the customer-to-product connection, AR is revolutionizing the shopping experience. For example, customers can use AR to try on clothes and interact with products from virtually anywhere. In turn, buyers have a more meaningful interaction with brands, leading to increased sales. 35mm incorporates both video commerce and AR to dramatically improve the shopping experience for buyers. As a result, 35mm is positioned to bring waves of loyal customers to the platform, allowing entrepreneurs and brands alike to be discovered. </p>
                            </div>
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>Why community counts</span>
                                <p className='pricing_data_para'>As a brand, establishing a unique selling position in global markets is incredibly challenging. Buyers can purchase similar products to your own at any time, anywhere. That’s why social commerce gives sellers an advantage. Innovative platforms like 35mm are establishing a democratized selling community where your identity is your competitive edge. The 35mm marketplace hinges on familiarity, trust, and relationship building. Sellers no longer need to rely on cold marketing tactics to spur sales. Instead, they can pique interest through social interaction.</p>
                            </div>
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>35mm understands exclusivity</span>
                                <p className='pricing_data_para'>As a seller, putting your products on a site that increases overall sales is critical. Exclusivity is just another way that 35mm supports your business.  We enable sellers to use pre-orders, waitlists, deadlines, and membership privileges to build an era of exclusivity around their products. In turn, cart abandonment rates go down while brand image goes up. Coupled with your unique identity as a seller, exclusivity on 35mm gives customers the confidence they need to purchase from your brand - time and time again.</p>
                            </div>
                            <div className="pricing_data_details">
                                <span className='pricing_data_span'>Join 35mm today for a more profitable tomorrow</span>
                                <p className='pricing_data_para'>Are you ready to join the new wave of eCommerce? By joining 35mm as a seller, you put your brand in the perfect position to capitalize on the predicted $84 billion U.S. social commerce market by 2024. The early adopter ‘ship’ has already sailed for big eCommerce stores, but 35mm is onboarding sellers as we speak. As an added bonus, our no nonsense pricing model lets you keep more of what you earn when compared to other eCommerce sites. With a mere 2.5% transaction fee and 4 tiers of subscriptions, you can focus on growing your business without paying exorbitant prices.</p>
                            </div>
                        </div>
                    </div>
                </Container>
            </section>
        </>
    )
}

export default Pricing
