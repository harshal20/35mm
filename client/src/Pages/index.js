import AboutUs from "./aboutUs/AboutUs";
import Blog from "./blog/Blog";
import Blogs from "./blog/Blogs";
import ContactUs from "./contact/ContactUs";
import Contact from "./contact/Contact";
import Compare from "./compare/Compare";
import LiveStreaming from "./liveStreaming/LiveStreaming";
import OurPolicies from "./our_policies/OurPolicies";
import OrderTracking from "./orderTracking/OrderTracking";
import Pricing from "./pricing/Pricing";
import Shopingcart from "./Shopingcart/Shopingcart/Shopingcart";

export {
  AboutUs,
  Blog,
  Blogs,
  ContactUs,
  Contact,
  Compare,
  LiveStreaming,
  OurPolicies,
  OrderTracking,
  Pricing,
  Shopingcart,
};
