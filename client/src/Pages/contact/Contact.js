import React from "react";
import { Breadcrumb, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Title from "../../component/layout/Title";
import { data } from "../../Data/data";
import Rating from "@mui/material/Rating";
import { contact_main, mm_contact } from "../../assets/images";

const Contact = () => {
  const { Contact_product } = data;

  return (
    <>
      <section className="contact">
        <div className="breadcrumb_pri">
          <div className="breadcrumb_pri_inner">
            <Breadcrumb>
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item active>Contact Us</Breadcrumb.Item>
            </Breadcrumb>
          </div>
        </div>
        <Container>
          <div className="contact_main">
            <div className="contact_tagline">
              <Row>
                <Col lg={6}>
                  <div className="contact_35mm">
                    <div className="contact_enterprice">
                      <img src={mm_contact} alt="img" />
                      <h6 className="pricing_head">35mm Enterprise</h6>
                      <p className="pricing_data_para">Tagline here...</p>
                    </div>
                    <div className="contact_about">
                      <h6 className="pricing_data_para">About</h6>
                      <p className="pricing_data_para">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean id interdum felis. Nunc eu ante id dui suscipit
                        tempor eu ut ligula. Sed quis dictum enim. Curabitur
                        ornare nisl non ante varius aliquet.
                      </p>
                    </div>
                  </div>
                </Col>
                <Col lg={6}>
                  <div className="contact_img">
                    <img src={contact_main} alt="img" />
                  </div>
                </Col>
              </Row>
            </div>
            <div className="contact_store">
              <div className="contact_store_title">
                <Title title_main={"Store Products"} />
              </div>
              <ul className="productdetail">
                {Contact_product.contact_product_content.map((value, index) => (
                  <li className="contact_store_product_inner" key={index}>
                    <Link to={"/"}>
                      <img src={value.image} alt="img" />
                      <h4 className="contact_product_head">{value.title}</h4>
                      <span className="contact_product_rating">
                        <Rating
                          name="half-rating-read"
                          defaultValue={5.0}
                          precision={4}
                          readOnly
                        />
                        <span className="contact_product_point">
                          {" "}
                          {value.rating}{" "}
                        </span>
                      </span>
                      <p className="contact_product_price">{value.price}</p>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </Container>
      </section>
    </>
  );
};

export default Contact;
