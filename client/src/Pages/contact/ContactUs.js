import React from "react";
import { Breadcrumb, Col, Container, Row } from "react-bootstrap";
import { Links } from "../../component/layout/Buttons";

const ContactUs = () => {
  return (
    <>
      <section className="contact_us">
        <div className="breadcrumb_pri">
          <div className="breadcrumb_pri_inner">
            <Breadcrumb>
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item active>Contact Us</Breadcrumb.Item>
            </Breadcrumb>
          </div>
        </div>
        <Container>
          <div className="contact_us_inner">
            <Row>
              <Col lg={4}>
                <div className="contact_us_help">
                  <div className="contact_us_help_inner">
                    <h6 className="pricing_head">Here to help</h6>
                    <p>Have a Question? You may find an answer in our FAQs.</p>
                    <p>But you can also contact us.</p>
                    <div className="contact_us_mail_main">
                      <h6 className="contact_us_mail">
                        Tweet us: @35mmSupport
                      </h6>
                      <h6 className="contact_us_mail">
                        Email us: hello@35mm.shop
                      </h6>
                    </div>
                  </div>
                  <div className="contact_us_opening">
                    <h6 className="contact_us_opening_hour">Opening Hours:</h6>
                    <div className="contact_us_day">
                      <p> Monday to Friday</p>
                      <p> 9:00 am - 6:00 pm</p>
                    </div>
                    <div className="contact_us_day">
                      <p> Saturday</p>
                      <p> 9:00 am - 6:00 pm</p>
                    </div>
                    <div className="contact_us_day">
                      <p> Sunday</p>
                      <p> 9:00 am - 4:00 pm</p>
                    </div>
                  </div>
                </div>
              </Col>
              <Col lg={8}>
                <div className="contact_drop">
                  <h6 className="pricing_head">Drop us a line</h6>
                  <div className="contact_us_input_fileds">
                    <form>
                      <div className="contact_us_input_inner">
                        <label htmlFor="name" className="contact_us_label">
                          First Name (required) *
                        </label>
                        <input
                          placeholder="Enter Your Name"
                          type="text"
                          name="name"
                          className="contact_us_input"
                          required
                        />
                      </div>
                      <div className="contact_us_input_inner">
                        <label htmlFor="email" className="contact_us_label">
                          Email Address (required) *
                        </label>
                        <input
                          placeholder="Enter Your Email Address"
                          type="email"
                          name="email"
                          className="contact_us_input"
                          required
                        />
                      </div>
                      <div className="contact_us_input_inner">
                        <label htmlFor="number" className="contact_us_label">
                          Phone Number (required) *
                        </label>
                        <input
                          placeholder="Enter Your Phone Number"
                          type="number"
                          name="number"
                          className="contact_us_input"
                          required
                        />
                      </div>
                      <div className="contact_us_input_inner">
                        <label htmlFor="name" className="contact_us_label">
                          First Name (required) *
                        </label>
                        <textarea
                          type="text"
                          name="name"
                          className="contact_us_textarea"
                          required
                        />
                      </div>
                      <Links link={"/"} buttons={"Submit"} />
                    </form>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </section>
    </>
  );
};

export default ContactUs;
