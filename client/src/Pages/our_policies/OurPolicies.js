import React from "react";
import { Accordion, Breadcrumb, Container, Tab, Tabs } from "react-bootstrap";

const OurPolicies = () => {
  return (
    <>
      <section className="our_policies">
        <div className="breadcrumb_pri">
          <div className="breadcrumb_pri_inner">
            <Breadcrumb>
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item active>Our Policies</Breadcrumb.Item>
            </Breadcrumb>
          </div>
        </div>
        <Container>
          <main className="our_policies_tag">
            <h2 className="pricing_head">Our Policies</h2>
            <div className="policies_tab">
              <Tabs
                defaultActiveKey="home"
                id="justify-tab-example"
                className="mb-3"
                justify
              >
                <Tab eventKey="home" title="Terms & Conditions">
                  <div className="policies_terms">
                    <div className="policies_terms_head">
                      <h5>Physical Goods Marketplace – Terms & Conditions</h5>
                      <p className="pricing_data_para">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean id interdum felis. Nunc eu ante id dui suscipit
                        tempor eu ut ligula. Sed quis dictum enim. Curabitur
                        ornare nisl non ante varius aliquet. Sed dui velit,
                        posuere id cursus sit amet, hendrerit ac nulla. Ut
                        finibus, quam ut sollicitudin facilisis, velit tellus
                        blandit risus, eu vulputate lectus nunc ac est. Aliquam
                        semper augue at massa malesuada aliquam. Donec euismod
                        condimentum tellus, in aliquet ante efficitur et.
                        Phasellus ut dapibus enim.
                      </p>
                    </div>
                    <div className="policies_terms_head">
                      <h6>Eligibility</h6>
                      <p className="pricing_data_para">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean id interdum felis. Nunc eu ante id dui suscipit
                        tempor eu ut ligula. Sed quis dictum enim. Curabitur
                        ornare nisl non ante varius aliquet. Sed dui velit,
                        posuere id cursus sit amet, hendrerit ac nulla. Ut
                        finibus, quam ut sollicitudin facilisis, velit tellus
                        blandit risus, eu vulputate lectus nunc ac est. Aliquam
                        semper augue at massa malesuada aliquam. Donec euismod
                        condimentum tellus, in aliquet ante efficitur et.
                        Phasellus ut dapibus enim.
                      </p>
                    </div>
                    <div className="policies_terms_head">
                      <h6>Marketplace Terms and Conditions</h6>
                      <p className="pricing_data_para">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean id interdum felis. Nunc eu ante id dui suscipit
                        tempor eu ut ligula. Sed quis dictum enim. Curabitur
                        ornare nisl non ante varius aliquet. Sed dui velit,
                        posuere id cursus sit amet, hendrerit ac nulla. Ut
                        finibus, quam ut sollicitudin facilisis, velit tellus
                        blandit risus, eu vulputate lectus nunc ac est. Aliquam
                        semper augue at massa malesuada aliquam. Donec euismod
                        condimentum tellus, in aliquet ante efficitur et.
                        Phasellus ut dapibus enim.
                      </p>
                      <p className="pricing_data_para">
                        Proin efficitur ex sit amet velit fermentum, quis
                        tincidunt leo pharetra. Maecenas placerat tellus justo,
                        commodo finibus justo tincidunt nec. Nunc luctus tellus
                        ac nulla congue egestas. Nullam a sollicitudin sapien, a
                        pulvinar lectus. Etiam eget pretium ipsum. Duis tempus
                        leo quam, sit amet ornare diam laoreet at. Nulla
                        facilisi. Sed vehicula est eu iaculis iaculis. Morbi
                        faucibus metus enim, nec consequat enim gravida in.
                        Curabitur leo lorem, pulvinar sed volutpat id, ultricies
                        viverra velit. Aenean porta laoreet justo, ac suscipit
                        ex bibendum nec. Nullam imperdiet nisi vitae neque
                        convallis, sed sodales neque maximus. Fusce tincidunt
                        eleifend nibh, ac pulvinar leo suscipit eget.
                        Suspendisse dapibus tortor iaculis, rhoncus justo at,
                        ultrices est. Mauris sit amet congue mi. Phasellus
                        cursus enim mollis fermentum consequat.
                      </p>
                      <p className="pricing_data_para">
                        Donec sed egestas augue. Phasellus non dui est.
                        Phasellus at viverra dui. Nulla congue congue eros, ut
                        viverra orci placerat sed. Morbi vitae leo at libero
                        viverra commodo. Ut lacinia porttitor sapien, at
                        efficitur arcu. Sed id justo tempus lectus laoreet
                        laoreet. Donec pretium rutrum purus sed imperdiet.
                        Nullam condimentum arcu a justo fringilla, tempus
                        iaculis felis lobortis. Vestibulum auctor scelerisque
                        pulvinar. Aenean tincidunt in eros eget blandit.
                        Praesent nec urna orci. Vestibulum ante ipsum primis in
                        faucibus orci luctus et ultrices posuere cubilia curae;
                        Donec quis magna pharetra, dapibus risus a, semper sem.
                        Suspendisse sodales eros id feugiat elementum. Maecenas
                        laoreet ipsum nunc, sed porttitor felis tristique ac.
                      </p>
                      <p className="pricing_data_para">
                        Nullam pharetra gravida lorem, quis laoreet risus
                        lacinia at. Nunc cursus magna tortor, at ultrices urna
                        iaculis et. Etiam lacinia a justo in facilisis. Cras
                        gravida pretium orci. Nullam scelerisque nec turpis
                        vitae sagittis. Donec id tempus tortor, id pulvinar
                        justo. Sed auctor vestibulum convallis. Duis suscipit
                        mattis sem, id sodales est. Quisque sed nunc lorem. Cras
                        vulputate orci lorem, pharetra tempus velit sagittis in.
                      </p>
                      <p className="pricing_data_para">
                        Maecenas ut enim sed justo venenatis luctus. Vivamus
                        bibendum cursus mauris, at hendrerit orci interdum nec.
                        Morbi tempus mollis neque finibus luctus. Praesent
                        accumsan porta condimentum. Cras mollis nec ipsum et
                        pellentesque. Nunc quis tincidunt justo. Ut pulvinar
                        nibh quis fermentum viverra. Proin laoreet convallis
                        urna vitae sagittis. Mauris eget lacinia quam. In et
                        erat arcu. Curabitur finibus commodo dolor vel
                        pellentesque. Fusce sed libero lectus.
                      </p>
                    </div>
                    <div className="policies_terms_head">
                      <h6>
                        Assignment; Restrictions on Information Sharing; Privacy
                        applicable to Marketplace Terms
                      </h6>
                      <p className="pricing_data_para">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean id interdum felis. Nunc eu ante id dui suscipit
                        tempor eu ut ligula. Sed quis dictum enim. Curabitur
                        ornare nisl non ante varius aliquet. Sed dui velit,
                        posuere id cursus sit amet, hendrerit ac nulla. Ut
                        finibus, quam ut sollicitudin facilisis, velit tellus
                        blandit risus, eu vulputate lectus nunc ac est. Aliquam
                        semper augue at massa malesuada aliquam. Donec euismod
                        condimentum tellus, in aliquet ante efficitur et.
                        Phasellus ut dapibus enim.
                      </p>
                    </div>
                    <div className="policies_terms_head">
                      <h6>Cashback</h6>
                      <p className="pricing_data_para">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean id interdum felis. Nunc eu ante id dui suscipit
                        tempor eu ut ligula. Sed quis dictum enim. Curabitur
                        ornare nisl non ante varius aliquet. Sed dui velit,
                        posuere id cursus sit amet, hendrerit ac nulla. Ut
                        finibus, quam ut sollicitudin facilisis, velit tellus
                        blandit risus, eu vulputate lectus nunc ac est. Aliquam
                        semper augue at massa malesuada aliquam. Donec euismod
                        condimentum tellus, in aliquet ante efficitur et.
                        Phasellus ut dapibus enim.
                      </p>
                    </div>
                    <div className="policies_terms_head">
                      <h6>Quick Response</h6>
                      <p className="pricing_data_para">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean id interdum felis. Nunc eu ante id dui suscipit
                        tempor eu ut ligula. Sed quis dictum enim. Curabitur
                        ornare nisl non ante varius aliquet. Sed dui velit,
                        posuere id cursus sit amet, hendrerit ac nulla. Ut
                        finibus, quam ut sollicitudin facilisis, velit tellus
                        blandit risus, eu vulputate lectus nunc ac est. Aliquam
                        semper augue at massa malesuada aliquam. Donec euismod
                        condimentum tellus, in aliquet ante efficitur et.
                        Phasellus ut dapibus enim.
                      </p>
                    </div>
                  </div>
                </Tab>
                <Tab eventKey="profile" title="Return Privacy">
                  <div className="policies_terms_head">
                    <h6>Quick Response</h6>
                    <p className="pricing_data_para">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aenean id interdum felis. Nunc eu ante id dui suscipit
                      tempor eu ut ligula. Sed quis dictum enim. Curabitur
                      ornare nisl non ante varius aliquet. Sed dui velit,
                      posuere id cursus sit amet, hendrerit ac nulla. Ut
                      finibus, quam ut sollicitudin facilisis, velit tellus
                      blandit risus, eu vulputate lectus nunc ac est. Aliquam
                      semper augue at massa malesuada aliquam. Donec euismod
                      condimentum tellus, in aliquet ante efficitur et.
                      Phasellus ut dapibus enim.
                    </p>
                  </div>
                </Tab>
                <Tab eventKey="longer-tab" title="FAQ's">
                  <div className="policies_terms">
                    <p className="pricing_data_para">
                      This is an approximate conversion table to help you find
                      your size. If you have already purchased an item by our
                      brand, we recommend you select the same size as indicated
                      on its label.
                    </p>
                    <div className="faqs_accordian">
                      <Accordion defaultActiveKey="0">
                        <Accordion.Item eventKey="0">
                          <Accordion.Header>
                            What are shipping times and costs?
                          </Accordion.Header>
                          <Accordion.Body>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aenean id interdum felis. Nunc eu ante id dui
                            suscipit tempor eu ut ligula. Sed quis dictum enim.
                            Curabitur ornare nisl non ante varius aliquet. Sed
                            dui velit, posuere id cursus sit amet, hendrerit ac
                            nulla. Ut finibus, quam ut sollicitudin facilisis,
                            velit tellus blandit risus, eu vulputate lectus nunc
                            ac est. Aliquam semper augue at massa malesuada
                            aliquam. Donec euismod condimentum tellus, in
                            aliquet ante efficitur et. Phasellus ut dapibus
                            enim.
                          </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="1">
                          <Accordion.Header>
                            What Payment method can I use?
                          </Accordion.Header>
                          <Accordion.Body>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aenean id interdum felis. Nunc eu ante id dui
                            suscipit tempor eu ut ligula. Sed quis dictum enim.
                            Curabitur ornare nisl non ante varius aliquet. Sed
                            dui velit, posuere id cursus sit amet, hendrerit ac
                            nulla. Ut finibus, quam ut sollicitudin facilisis,
                            velit tellus blandit risus, eu vulputate lectus nunc
                            ac est. Aliquam semper augue at massa malesuada
                            aliquam. Donec euismod condimentum tellus, in
                            aliquet ante efficitur et. Phasellus ut dapibus
                            enim.
                          </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="2">
                          <Accordion.Header>
                            What is your exchages, Returns and refunds policy?
                          </Accordion.Header>
                          <Accordion.Body>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aenean id interdum felis. Nunc eu ante id dui
                            suscipit tempor eu ut ligula. Sed quis dictum enim.
                            Curabitur ornare nisl non ante varius aliquet. Sed
                            dui velit, posuere id cursus sit amet, hendrerit ac
                            nulla. Ut finibus, quam ut sollicitudin facilisis,
                            velit tellus blandit risus, eu vulputate lectus nunc
                            ac est. Aliquam semper augue at massa malesuada
                            aliquam. Donec euismod condimentum tellus, in
                            aliquet ante efficitur et. Phasellus ut dapibus
                            enim.
                          </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="3">
                          <Accordion.Header>
                            What is your exchages, Returns and refunds policy?
                          </Accordion.Header>
                          <Accordion.Body>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aenean id interdum felis. Nunc eu ante id dui
                            suscipit tempor eu ut ligula. Sed quis dictum enim.
                            Curabitur ornare nisl non ante varius aliquet. Sed
                            dui velit, posuere id cursus sit amet, hendrerit ac
                            nulla. Ut finibus, quam ut sollicitudin facilisis,
                            velit tellus blandit risus, eu vulputate lectus nunc
                            ac est. Aliquam semper augue at massa malesuada
                            aliquam. Donec euismod condimentum tellus, in
                            aliquet ante efficitur et. Phasellus ut dapibus
                            enim.
                          </Accordion.Body>
                        </Accordion.Item>
                      </Accordion>
                    </div>
                  </div>
                </Tab>
              </Tabs>
            </div>
          </main>
        </Container>
      </section>
    </>
  );
};

export default OurPolicies;
