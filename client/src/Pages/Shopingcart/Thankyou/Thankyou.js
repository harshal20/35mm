import React from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import "./Thankyou.css";
import trueimage from "../../../assets/images/endimg.png";
import { cart1, cart2 } from '../../../assets/images';


const Thankyou = () => {
    return (
        <>
            <section className='Thankyou'>

                <div className="bg">
                    <Breadcrumb>
                        <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                        <Breadcrumb.Item href="/">Cart</Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <Container>
                    <Row>

                        <Col lg={9}>
                            <div className="thankyou">
                                <div className="thankdetail">
                                    <div className="thankspage">
                                        <span>
                                            <img src={trueimage} alt="" /></span>
                                        <div className="thanks">
                                            <h4>Thank You!</h4>
                                            <p>Your Order has been received.</p>
                                        </div>
                                    </div>
                                    <ul className='orderdetail'>
                                        <li>
                                            <h4>Order Number:</h4>
                                            <p>2600</p>
                                        </li>
                                        <li>
                                            <h4>Date:</h4>
                                            <p>24 Aug, 2020</p>
                                        </li><li>
                                            <h4>Total:</h4>
                                            <p>$930.00</p>
                                        </li><li>
                                            <h4>Payment Method:</h4>
                                            <p>Cash on Delivery</p>
                                        </li>
                                    </ul>

                                    <div className="orderss">
                                        <h3>Order Details</h3>
                                        <ul className="producthis">
                                            <li>
                                                <div className="dress">
                                                    <img src={cart1} alt="" />
                                                    <h3>Long Strappy Dress</h3>
                                                </div>
                                                <h4>$300.00</h4>
                                            </li>
                                            <li>
                                                <div className="dress">
                                                    <img src={cart2} alt="" />
                                                    <h3>35mm Unique Bag - Yellow</h3>
                                                </div>
                                                <h4>$600.00</h4>
                                            </li>
                                        </ul>
                                    </div>
                                    <ul className="totalright">
                                        <li>    <h4>Subtotal:</h4>
                                            <span>$900.00</span>
                                        </li>
                                        <li> <h4>Shipping:</h4>
                                            <span>$30.00</span></li>
                                        <li> <h4>Total:</h4>
                                            <span>$930.00</span></li>
                                    </ul>

                                </div>
                            </div>
                        </Col>
                        <Col lg={3}>

                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Thankyou