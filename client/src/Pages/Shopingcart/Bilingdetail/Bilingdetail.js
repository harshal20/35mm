import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import { Link } from "react-router-dom";
import "./Billingdetail.css";

const Bilingdetail = () => {
  return (
    <>
      <section className="billonfo">
        <div className="bg">
          <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item href="/">Cart</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <Container>
          <Row>
            <Col lg={9}>
              <div className="shopingcart detail">
                <h3>Billing Details</h3>
                <form action="">
                  <div className="namesdetail">
                    <div className="name">
                      <label htmlFor="First Name">First Name</label> <br />
                      <input type="text" />
                    </div>
                    <div className="name">
                      <label htmlFor="Last Name">Last Name</label> <br />
                      <input type="text" />
                    </div>
                  </div>
                  <div className="namesdetail">
                    <div className="name">
                      <label htmlFor="Company name (optional)">
                        Company name (optional)
                      </label>{" "}
                      <br />
                      <input type="text" />
                    </div>
                    <div className="name ">
                      <label htmlFor="Country*">Country*</label> <br />
                      <select name="Country" id="United States">
                        <option value="United States">United States</option>
                        <option value="United States">United States</option>
                        <option value="United States">United States</option>
                      </select>
                    </div>
                  </div>
                  <div className="namesdetail">
                    <div className="name">
                      <label htmlFor="Street Address*">Street Address*</label>{" "}
                      <br />
                      <input
                        type="text"
                        placeholder="House Number and street Name*"
                      />
                    </div>
                    <div className="name">
                      <input
                        type="text"
                        placeholder="Apartment, suite, unit etc. (optional)"
                      />
                    </div>
                  </div>
                  <div className="namesdetail">
                    <div className="name">
                      <label htmlFor="Town / City*">Town / City*</label> <br />
                      <input type="text" placeholder="" />
                    </div>
                    <div className="name">
                      <label htmlFor="State / County*">State / County*</label>{" "}
                      <br />
                      <select
                        name="Country"
                        id="Select an option..."
                        value="Select an option..."
                      >
                        <option value="Select an option...">
                          Select an option...
                        </option>
                        <option value="">Surat</option>
                        <option value="">Vadodara</option>
                        <option value="">Amhdavad</option>
                      </select>
                    </div>
                  </div>
                  <div className="namesdetail postcode">
                    <div className="name">
                      <label htmlFor="Postcode / ZIP*">Postcode / ZIP*</label>{" "}
                      <br />
                      <input type="text" placeholder="" />
                    </div>
                  </div>

                  <div className="namesdetail account">
                    <div className="name">
                      <label htmlFor="Email Address*">Email Address*</label>{" "}
                      <br />
                      <input type="text" placeholder="" />
                    </div>
                    <div className="name">
                      <label for="quantity">Phone Number*</label> <br />
                      <input type="number" name="quantity" />
                    </div>
                  </div>
                  <div className="create">
                    <input type="checkbox" />
                    <span> Create an account?</span>
                    <div className="namesdetail password">
                      <div className="name">
                        <label htmlFor="Password*">Password*</label> <br />
                        <input type="text" placeholder="" />
                      </div>
                      <div className="name">
                        <label htmlFor="Confirm Password*">
                          Confirm Password*
                        </label>{" "}
                        <br />
                        <input type="text" placeholder="" />
                      </div>
                    </div>
                  </div>
                  <div className="create">
                    <div className="ship">
                      <input type="checkbox" />
                      <span> Ship to a different address?</span>
                    </div>
                    <div className="name">
                      <label htmlFor="">Order Notes (optional)</label> <br />
                      <textarea type="text" placeholder="" rows={5} />
                    </div>
                  </div>
                </form>
              </div>
            </Col>
            <Col lg={3}>
              <div className="cartdetailtotal bill">
                <h3>Cart Details</h3>
                <div className="subtotaldetail">
                  <div className="subtotal">
                    <h4>Subtotal</h4>
                    <span>$900.00</span>
                  </div>
                  <div className="subtotal">
                    <h4>Shipping Change</h4>
                    <span>$30.00</span>
                  </div>
                  <div className="newtotal totals">
                    <h4>Total</h4>
                    <span>$930.00</span>
                  </div>
                </div>
                <Link to="">Place Order</Link>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Bilingdetail;
