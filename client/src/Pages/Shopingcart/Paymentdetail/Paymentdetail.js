import React from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { Link } from 'react-router-dom';
import "./Paymentdetail.css"


const Paymentdetail = () => {
    return (
        <>
            <section className='billonfo'>

                <div className="bg">
                    <Breadcrumb>
                        <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                        <Breadcrumb.Item href="/">Cart</Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <Container>
                    <Row>

                        <Col lg={9}>
                            <div className="shopingcart detail payment">
                                <h3>Payment Method</h3>


                                <form action="">
                                    <div className="paymentscard">
                                        <div className="name">
                                            <span className='paymrntstatus'><svg width="24" height="16" viewBox="0 0 24 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M2.9479 0H21.5521C22.904 0 24 1.09595 24 2.44791V13.2188C24 14.5707 22.904 15.6667 21.5521 15.6667H2.9479C1.59596 15.6667 0.5 14.5708 0.5 13.2188V2.44794C0.5 1.09595 1.59596 0 2.9479 0Z" fill="#2196F3" />
                                                <rect x="0.5" y="2.9375" width="23.5" height="3.91666" fill="#455A64" />
                                                <path d="M7.84375 10.7708H3.92709C3.65671 10.7708 3.4375 10.5516 3.4375 10.2812C3.4375 10.0109 3.65671 9.79166 3.92709 9.79166H7.84375C8.11414 9.79166 8.33336 10.0109 8.33336 10.2812C8.33336 10.5516 8.11414 10.7708 7.84375 10.7708Z" fill="#FAFAFA" />
                                                <path d="M10.7813 12.7292H3.92709C3.65671 12.7292 3.4375 12.51 3.4375 12.2396C3.4375 11.9692 3.65671 11.75 3.92709 11.75H10.7813C11.0516 11.75 11.2709 11.9692 11.2709 12.2396C11.2709 12.51 11.0516 12.7292 10.7813 12.7292Z" fill="#FAFAFA" />
                                            </svg>
                                            </span>

                                            <select name="" id="Debit card">
                                                <option value="Debit card">Debit card</option>
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                            </select>
                                        </div>
                                        <div className="paymentscard">
                                            <input type="text" placeholder='Card Number' />
                                        </div>
                                        <div className="paymentscard">
                                            <input type="text" placeholder='Holder Card' />
                                        </div>
                                        <div className="paymentscard">
                                            <div className="expire">
                                                <input type="text" placeholder='MM/YY' />
                                                <span className='paym'><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M14 2.6H17.2C17.6418 2.6 18 2.95817 18 3.4V16.2C18 16.6418 17.6418 17 17.2 17H2.8C2.35817 17 2 16.6418 2 16.2V3.4C2 2.95817 2.35817 2.6 2.8 2.6H6V1H7.6V2.6H12.4V1H14V2.6ZM12.4 4.2H7.6V5.8H6V4.2H3.6V7.4H16.4V4.2H14V5.8H12.4V4.2ZM16.4 9H3.6V15.4H16.4V9Z" fill="#1190CB" />
                                                </svg>
                                                </span>
                                            </div>
                                            <div className="expire">
                                                <input type="text" placeholder='CVV' />
                                                <span className='paym'><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10 18C5.5816 18 2 14.4184 2 10C2 5.5816 5.5816 2 10 2C14.4184 2 18 5.5816 18 10C18 14.4184 14.4184 18 10 18ZM10 16.4C13.5346 16.4 16.4 13.5346 16.4 10C16.4 6.46538 13.5346 3.6 10 3.6C6.46538 3.6 3.6 6.46538 3.6 10C3.6 13.5346 6.46538 16.4 10 16.4ZM9.2 6H10.8V7.6H9.2V6ZM9.2 9.2H10.8V14H9.2V9.2Z" fill="#1C2D41" />
                                                </svg>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="name">
                                            <span className='paymrntstatus'><svg xmlns="http://www.w3.org/2000/svg" width="15.079" height="18.055" viewBox="0 0 15.079 18.055">
                                                <g id="paypal_1_" data-name="paypal (1)" transform="translate(-42.193)">
                                                    <path id="Path_134" data-name="Path 134" d="M55.829,4.665a6.2,6.2,0,0,0,.047-.758A3.907,3.907,0,0,0,51.969,0H45.44a.841.841,0,0,0-.829.7L42.205,15a.841.841,0,0,0,.829.981h2.388a.848.848,0,0,0,.836-.7l.011-.066h0l-.333,1.978a.741.741,0,0,0,.731.864h2.089a.741.741,0,0,0,.731-.618l.593-3.527a1.263,1.263,0,0,1,1.245-1.053h.549a5.4,5.4,0,0,0,5.4-5.4A3.423,3.423,0,0,0,55.829,4.665Z" transform="translate(0)" fill="#002987" />
                                                    <path id="Path_135" data-name="Path 135" d="M157.958,132.28a6.151,6.151,0,0,1-6.1,5.392h-1.822a.914.914,0,0,0-.878.661l-1.089,6.473a.741.741,0,0,0,.731.864h2.089a.741.741,0,0,0,.731-.618l.593-3.527a1.263,1.263,0,0,1,1.245-1.053H154a5.4,5.4,0,0,0,5.4-5.4h0A3.423,3.423,0,0,0,157.958,132.28Z" transform="translate(-102.129 -127.615)" fill="#0085cc" />
                                                    <path id="Path_136" data-name="Path 136" d="M180.111,120.329h1.822a6.151,6.151,0,0,0,6.1-5.392,3.412,3.412,0,0,0-1.985-.634H181.3a1.109,1.109,0,0,0-1.094.925l-.969,5.762A.914.914,0,0,1,180.111,120.329Z" transform="translate(-132.207 -110.272)" fill="#00186a" />
                                                </g>
                                            </svg>

                                            </span>

                                            <select name="" id="Paypal">
                                                <option value="Paypal">Paypal</option>
                                                <option value="Upi">Upi</option>
                                                <option value="Paypal">Paypal</option>
                                            </select>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </Col>
                        <Col lg={3}>
                            <div className="cartdetailtotal bill">
                                <h3>Cart Details</h3>
                                <div className='subtotaldetail'>
                                    <div className='subtotal'>
                                        <h4>Subtotal</h4>
                                        <span>$900.00</span>
                                    </div>
                                    <div className='subtotal'>
                                        <h4>Shipping Change</h4>
                                        <span>$30.00</span>
                                    </div>
                                    <div className='newtotal totals'>
                                        <h4>Total</h4>
                                        <span>$930.00</span>
                                    </div>
                                </div>
                                <Link to="">Place Order</Link>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Paymentdetail