import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { Link } from 'react-router-dom';
import { cart1, cart2 } from '../../../assets/images';
import "./Shopingcart.css"


const Shopingcart = () => {
    return (
        <>
            <section className='shopingcartdetail'>
                <div className="bg">
                    <Breadcrumb>
                        <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                        <Breadcrumb.Item href="/">Cart</Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <Container>
                    <Row>
                        <Col lg={9}>
                            <div className="shopingcart">
                                <h3>Shopping Cart</h3>
                                <div className="cartadds">
                                    <div className="cartadd">
                                        <div className="cartdetail">
                                            <img src={cart1} alt="" />
                                            <h4>Long Strappy Dress</h4>
                                        </div>
                                        <span>$300.00</span>
                                        <input type="text" value="1" />
                                        <div className="delete">
                                            <span>$300.00</span>
                                            <Link>
                                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M18 5V19C18 19.5523 17.5523 20 17 20H3C2.44772 20 2 19.5523 2 19V5H0V3H20V5H18ZM4 5V18H16V5H4ZM5 0H15V2H5V0ZM9 8H11V15H9V8Z" fill="#FF5151" />
                                                </svg>
                                            </Link>
                                        </div>
                                    </div>
                                    <div className="cartadd">
                                        <div className="cartdetail">
                                            <img src={cart2} alt="" />
                                            <h4>35mm Unique Bag - Yellow</h4>
                                        </div>
                                        <span>$300.00</span>
                                        <input type="text" value="1" />

                                        <div className="delete">
                                            <span>$300.00</span>
                                            <Link>
                                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M18 5V19C18 19.5523 17.5523 20 17 20H3C2.44772 20 2 19.5523 2 19V5H0V3H20V5H18ZM4 5V18H16V5H4ZM5 0H15V2H5V0ZM9 8H11V15H9V8Z" fill="#FF5151" />
                                                </svg>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="coupne">
                                    <div className="apply">
                                        <input type="text" placeholder='Coupon Code' />
                                        <Link to="/">Apply</Link>
                                    </div>
                                    <div className="continue">
                                        <Link to="/Bilingdetail">Continue Shopping</Link>
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col lg={3}>

                            <div className="cartdetailtotal">
                                <h3>Cart Details</h3>
                                <div className='subtotaldetail'>
                                    <div className='subtotal'>
                                        <h4>Subtotal</h4>
                                        <span>$900.00</span>
                                    </div>
                                    <div className='subtotal'>
                                        <h4>Shipping Change</h4>
                                        <span>$30.00</span>
                                    </div>
                                    <div className='subtotal totals'>
                                        <h4>Total</h4>
                                        <span>$930.00</span>
                                    </div>
                                </div>
                                <Link to="">Proceed to Checkout</Link>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Shopingcart;