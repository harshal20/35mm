import React from 'react'
import { Breadcrumb, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { blogs_main, blogs_profile, recent1, recent2, recent3 } from '../../assets/images'

const Blogs = () => {
    return (
        <>
            <section className='blogs'>
                <div className="breadcrumb_pri">
                    <div className="breadcrumb_pri_inner">
                        <Breadcrumb>
                            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                            <Breadcrumb.Item active>Blog</Breadcrumb.Item>
                        </Breadcrumb>
                    </div>
                </div>
                <Container>
                    <main className='blogs_main_tag'>
                        <aside className='blogs_aside'>
                            <div className="blogs_profile">
                                <img src={blogs_profile} alt="img" />
                                <p className='blogs_profile_name'>Alex suprn</p>
                                <p className='blogs_para'>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad, quis nostrud.</p>
                                <div className="blogs_btn">
                                    <Link to={'/'}>Alex suprn</Link>
                                </div>
                            </div>
                            <div className="blogs_categosris">
                                <h6 className='blogs_category'>Categories</h6>
                                <div className="blogs_category_link">
                                    <Link className='blogs_para'>Beachwear</Link>
                                    <Link className='blogs_para'>Fashion</Link>
                                    <Link className='blogs_para'>Masonry</Link>
                                    <Link className='blogs_para'>Sunglasses</Link>
                                    <Link className='blogs_para'>Winter</Link>
                                </div>
                            </div>
                            <div className="blogs_recent">
                                <h6 className='blogs_category'>Recent Posts</h6>
                                <div className="blogs_recent_main">
                                    <div className="blogs_recent_img">
                                        <img src={recent1} alt="img" />
                                    </div>
                                    <div className="blogs_recent_detail">
                                        <p className='blogs_recent_tit'>Bags For Understanding Fashion's
                                            Fanny Pack Craze</p>
                                        <p className='blogs_para'>March 07 2019 Posted by Sofia Flyerson</p>
                                    </div>
                                </div>
                                <div className="blogs_recent_main">
                                    <div className="blogs_recent_img">
                                        <img src={recent2} alt="img" />
                                    </div>
                                    <div className="blogs_recent_detail">
                                        <p className='blogs_recent_tit'>Bags For Understanding Fashion's
                                            Fanny Pack Craze</p>
                                        <p className='blogs_para'>March 07 2019 Posted by Alexa Jeans</p>
                                    </div>
                                </div>
                                <div className="blogs_recent_main">
                                    <div className="blogs_recent_img">
                                        <img src={recent3} alt="img" />
                                    </div>
                                    <div className="blogs_recent_detail">
                                        <p className='blogs_recent_tit'>Bags For Understanding Fashion's
                                            Fanny Pack Craze</p>
                                        <p className='blogs_para'>March 07 2019 Posted by Mike Wheeler</p>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <div className="blogs_main">
                            <h1 className='blogs_head'>Why exclusivity creates profit</h1>
                            <p className='pricing_data_para'>January 15, 2019 posted by Pitter Parker</p>
                            <img src={blogs_main} alt="img" />
                            <h3 className='blogs_inner_head'>1. What is video commerce?</h3>
                            <p className='pricing_data_para pricing_data_para_inner'>
                                We all want a piece of the profitability pie when it comes down to being entrepreneurs. Ecommerce sales reached $3.53 trillion globally, and revenue is projected to reach $6.54 trillion worldwide in 2022. So, of course, we want a piece of that pie. Our goal is to make money. Simple as that. As marketers, we want our consumers to develop FOMO when it comes to selling products. The best way to do that is by setting exclusivity around the products you sell. To enforce the exclusivity strategy, brands must limit supply.
                            </p>
                            <p className='pricing_data_para'>
                                By limiting supply, consumers spend less time pondering whether to purchase a product or not. When we don’t enact exclusivity, buyers spend more time trying to decide whether to buy it or not. That leads to more cart abandonments and missed profit. With limited supply, it can drive up demand because consumers are making purchases based on instinct instead of logic. Thus it leads to more revenue. Business2Community list three effective strategies eCommerce sellers can
                                use to build up exclusivity. Those three strategies include:
                            </p>
                            <ul className="blogs_whitlist">
                                <li>
                                    <h6 className='blogs_inner_head'>Waitlists and pre-order
                                        lists</h6>
                                    <p className='pricing_data_para'>Limit the available supply and create a first-come, first-serve atmosphere with all new product launches.</p>
                                </li>
                                <li>
                                    <h6 className='blogs_inner_head'>Deadlines</h6>
                                    <p className='pricing_data_para'>When offers are only around for a limited time, create urgency in customers, and
                                        make them feel like they are getting a deal that others won’t be getting</p>
                                </li>
                                <li>
                                    <h6 className='blogs_inner_head'>Membership</h6>
                                    <p className='pricing_data_para'>Create a members-only group for your customers to join that gives them special
                                        privileges and offers that the general public is not entitled to.</p>
                                </li>
                            </ul>
                            <p className='pricing_data_para pricing_data_para_inner'>As sellers, we want to ensure that the products we put up for sale are worthy of customers opening up their wallets. That is why brands must develop distinct identities that make their products seem exclusive, even though many other competitors are similar and serve the same purpose. It is one of the most proactive ways to set up and enhance your competitive edge as a business. You get your products sold out faster.</p>
                            <p className='pricing_data_para'> At 35mm, we create interactive and new ways of shopping. We aim to change the way buyers shop and how sellers sell. We urge our sellers to sell in limited supply and follow our marketing guidelines. We give our sellers the ability to market their limited supply to 35mm buyers every day. Email newsletters inform our buyers, live streams, AR, and other video content on new items brands on 35mm are selling. Start selling today for free on 35mm. Welcome abroad.
                            </p>
                            <div className="blogs_prev_next">
                                <div className="blogs_prev">
                                    <Link to={'/'}>
                                        <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M2.82832 6.99977L7.77832 11.9498L6.36432 13.3638L0.000320435 6.99977L6.36432 0.635769L7.77832 2.04977L2.82832 6.99977Z" fill="#1190CB" />
                                        </svg>
                                    </Link>
                                    <p>Social Commerce & Community</p>
                                </div>
                                <div className="blogs_prev">
                                    <p> What is AR and why eComm should embrace it?</p>
                                    <Link to={'/'}>
                                        <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5.17168 7.00023L0.22168 2.05023L1.63568 0.63623L7.99968 7.00023L1.63568 13.3642L0.22168 11.9502L5.17168 7.00023Z" fill="#1190CB" />
                                        </svg>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </main>
                </Container>
            </section>
        </>
    )
}

export default Blogs
