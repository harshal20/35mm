import React from 'react'
import { Breadcrumb, Col, Container, Row } from 'react-bootstrap'
import { data } from '../../Data/data'

const Blog = () => {

    const { Blogs } = data;

    return (
        <>
            <section className='blog'>
                <div className="breadcrumb_pri">
                    <div className="breadcrumb_pri_inner">
                        <Breadcrumb>
                            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                            <Breadcrumb.Item active>Blog</Breadcrumb.Item>
                        </Breadcrumb>
                    </div>
                </div>
                <Container>
                    <div className="blog_main">
                        <Row>
                            {Blogs.blog_contant.map((value, i) => (
                                <Col lg={6} key={i}>
                                    <div className="blog_main_data">
                                        <span className='pricing_para'>{value.title}</span>
                                        <p className='pricing_data_para'>{value.subtitle}</p>
                                        <img src={value.image} alt="" />
                                    </div>
                                </Col>
                            ))}
                        </Row>
                    </div>
                </Container>
            </section>
        </>
    )
}

export default Blog
