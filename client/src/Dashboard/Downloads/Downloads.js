import React from "react";
import "./Downloads.css";
import Sidebar from "../Sidebar/Sidebar";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import orderimg from "../../assets/images/order/order1.png";
import proimg2 from "../../assets/images/product/purchasehistory/productimg2.png";
import Rating from "@mui/material/Rating";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

const Downloads = () => {
  return (
    <div className="downloads">
      <Container>
        <Breadcrumb>
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>

          <Breadcrumb.Item active>sunglasses</Breadcrumb.Item>
        </Breadcrumb>
        <div className="maindash">
          <div className="sidebar">
            <Sidebar></Sidebar>
          </div>
          <div className="dashdetail">
            <h2>Downloads</h2>

            <table className="protable">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Order ID</th>
                  <th>Date Purchase</th>
                  <th>Receipt download</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div className="proddetail">
                      <img src={orderimg} alt="" />
                      <div className="proinner">
                        <h5>Long Kurta and Churidar </h5>
                        <div className="ratingstar">
                          <Rating
                            name="half-rating-read"
                            defaultValue={4.5}
                            precision={0.5}
                            readOnly
                            size="small"
                          />
                          4.0
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span className="pricedol">OD117216332413925000</span>
                  </td>
                  <td>
                    <span className="pricedate">Aug 24, 2020</span>
                  </td>
                  <td>
                    <div className="downloadtbn">
                      <Link className="downloadbtntbn" to="">
                        <span>
                          <svg
                            width="21"
                            height="20"
                            viewBox="0 0 21 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M11.3337 10.8337V15.4878L12.8571 13.9645L14.0362 15.1437L10.5004 18.6787L6.96457 15.1437L8.14373 13.9645L9.66707 15.4878V10.8337H11.3337ZM10.5004 1.66699C11.9312 1.66706 13.3121 2.19292 14.3806 3.14459C15.449 4.09626 16.1305 5.40738 16.2954 6.82866C17.3323 7.11142 18.2368 7.74958 18.851 8.6316C19.4651 9.51362 19.7497 10.5834 19.6551 11.654C19.5605 12.7246 19.0927 13.7279 18.3334 14.4885C17.5741 15.2492 16.5716 15.7188 15.5012 15.8153L15.5004 14.167C15.5017 12.8563 14.9883 11.5975 14.0707 10.6615C13.1531 9.72561 11.9047 9.18743 10.5942 9.16285C9.28369 9.13827 8.01595 9.62926 7.06389 10.5301C6.11182 11.431 5.55157 12.6697 5.50373 13.9795L5.5004 14.167V15.8153C4.42994 15.7189 3.4274 15.2494 2.66801 14.4888C1.90862 13.7282 1.44067 12.725 1.34596 11.6543C1.25125 10.5837 1.5358 9.5139 2.14987 8.6318C2.76395 7.74971 3.66849 7.11148 4.7054 6.82866C4.87016 5.40731 5.55157 4.09608 6.62005 3.14438C7.68853 2.19268 9.06953 1.66688 10.5004 1.66699V1.66699Z"
                              fill="#09121F"
                            />
                          </svg>
                        </span>
                        Download
                      </Link>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div className="proddetail">
                      <img src={orderimg} alt="" />
                      <div className="proinner">
                        <h5>Long Kurta and Churidar </h5>
                        <div className="ratingstar">
                          <Rating
                            name="half-rating-read"
                            defaultValue={4.5}
                            precision={0.5}
                            readOnly
                            size="small"
                          />
                          4.0
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span className="pricedol">OD117216332413925000</span>
                  </td>
                  <td>
                    <span className="pricedate">Aug 24, 2020</span>
                  </td>
                  <td>
                    <div className="downloadtbn">
                      <Link className="downloadbtntbn" to="">
                        <span>
                          <svg
                            width="21"
                            height="20"
                            viewBox="0 0 21 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M11.3337 10.8337V15.4878L12.8571 13.9645L14.0362 15.1437L10.5004 18.6787L6.96457 15.1437L8.14373 13.9645L9.66707 15.4878V10.8337H11.3337ZM10.5004 1.66699C11.9312 1.66706 13.3121 2.19292 14.3806 3.14459C15.449 4.09626 16.1305 5.40738 16.2954 6.82866C17.3323 7.11142 18.2368 7.74958 18.851 8.6316C19.4651 9.51362 19.7497 10.5834 19.6551 11.654C19.5605 12.7246 19.0927 13.7279 18.3334 14.4885C17.5741 15.2492 16.5716 15.7188 15.5012 15.8153L15.5004 14.167C15.5017 12.8563 14.9883 11.5975 14.0707 10.6615C13.1531 9.72561 11.9047 9.18743 10.5942 9.16285C9.28369 9.13827 8.01595 9.62926 7.06389 10.5301C6.11182 11.431 5.55157 12.6697 5.50373 13.9795L5.5004 14.167V15.8153C4.42994 15.7189 3.4274 15.2494 2.66801 14.4888C1.90862 13.7282 1.44067 12.725 1.34596 11.6543C1.25125 10.5837 1.5358 9.5139 2.14987 8.6318C2.76395 7.74971 3.66849 7.11148 4.7054 6.82866C4.87016 5.40731 5.55157 4.09608 6.62005 3.14438C7.68853 2.19268 9.06953 1.66688 10.5004 1.66699V1.66699Z"
                              fill="#09121F"
                            />
                          </svg>
                        </span>
                        Download
                      </Link>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div className="proddetail">
                      <img src={orderimg} alt="" />
                      <div className="proinner">
                        <h5>Long Kurta and Churidar </h5>
                        <div className="ratingstar">
                          <Rating
                            name="half-rating-read"
                            defaultValue={4.5}
                            precision={0.5}
                            readOnly
                            size="small"
                          />
                          4.0
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span className="pricedol">OD117216332413925000</span>
                  </td>
                  <td>
                    <span className="pricedate">Aug 24, 2020</span>
                  </td>
                  <td>
                    <div className="downloadtbn">
                      <Link className="downloadbtntbn" to="">
                        <span>
                          <svg
                            width="21"
                            height="20"
                            viewBox="0 0 21 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M11.3337 10.8337V15.4878L12.8571 13.9645L14.0362 15.1437L10.5004 18.6787L6.96457 15.1437L8.14373 13.9645L9.66707 15.4878V10.8337H11.3337ZM10.5004 1.66699C11.9312 1.66706 13.3121 2.19292 14.3806 3.14459C15.449 4.09626 16.1305 5.40738 16.2954 6.82866C17.3323 7.11142 18.2368 7.74958 18.851 8.6316C19.4651 9.51362 19.7497 10.5834 19.6551 11.654C19.5605 12.7246 19.0927 13.7279 18.3334 14.4885C17.5741 15.2492 16.5716 15.7188 15.5012 15.8153L15.5004 14.167C15.5017 12.8563 14.9883 11.5975 14.0707 10.6615C13.1531 9.72561 11.9047 9.18743 10.5942 9.16285C9.28369 9.13827 8.01595 9.62926 7.06389 10.5301C6.11182 11.431 5.55157 12.6697 5.50373 13.9795L5.5004 14.167V15.8153C4.42994 15.7189 3.4274 15.2494 2.66801 14.4888C1.90862 13.7282 1.44067 12.725 1.34596 11.6543C1.25125 10.5837 1.5358 9.5139 2.14987 8.6318C2.76395 7.74971 3.66849 7.11148 4.7054 6.82866C4.87016 5.40731 5.55157 4.09608 6.62005 3.14438C7.68853 2.19268 9.06953 1.66688 10.5004 1.66699V1.66699Z"
                              fill="#09121F"
                            />
                          </svg>
                        </span>
                        Download
                      </Link>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Downloads;
