import React from 'react'
import './EditProfile.css'
import { Container } from 'react-bootstrap';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Row from 'react-bootstrap/Row';
import Tab from 'react-bootstrap/Tab';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import propic from '../../assets/images/profile/profilemain.png'
import Address from './address.js'


const EditProfile = () => {
    return (

        <>
            <div className="editprofile">
                <Container>
                    <Breadcrumb>
                        <Breadcrumb.Item href="/dashboard">Home</Breadcrumb.Item>

                        <Breadcrumb.Item active>Edit Profile  </Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="editprofiledetail">

                        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                            <Row>
                                <Col sm={3}>
                                    <div className="editprotitle">
                                        <h4>Edit Profile</h4>
                                    </div>
                                    <Nav variant="pills" className="flex-column">
                                        <Nav.Item>
                                            <Nav.Link eventKey="first">Personal Information</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="second">Manage Addresses</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="third">Change Password</Nav.Link>
                                        </Nav.Item>
                                    </Nav>
                                </Col>
                                <Col sm={9}>
                                    <Tab.Content>
                                        <Tab.Pane eventKey="first">
                                            <div className="prodetail">
                                                <div className="proimg">
                                                    <img src={propic} alt="" />
                                                    <div className="changeimg">
                                                        <h5>Profile Image</h5>
                                                        <div className="change">
                                                            <input type="file" />
                                                            <a href="#">Change</a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <Row>
                                                    <Col lg={6}>
                                                        <div className="pronamedetail">
                                                            <label>First Name</label>
                                                            <input type="text" value="Martin" />
                                                        </div>
                                                    </Col>
                                                    <Col lg={6}>
                                                        <div className="pronamedetail">
                                                            <label>First Name</label>
                                                            <input type="text" value="Martin" />
                                                        </div>
                                                    </Col>
                                                    <Col lg={6}>
                                                        <div className="pronamedetail malfemale">
                                                            <label>Gender</label>
                                                            <div className="selectmaingender">
                                                                <div className="selectmalefemale">
                                                                    <input type="radio" name='gender' value="Martin" id='male' />
                                                                    <label htmlFor='male'>Male</label>
                                                                </div>
                                                                <div className="selectmalefemale">
                                                                    <input type="radio" name='gender' value="Martin" id='female' />
                                                                    <label htmlFor='female'>Female</label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </Col>
                                                    <Col lg={6}>

                                                    </Col>
                                                    <Col lg={6}>
                                                        <div className="pronamedetail">
                                                            <label>First Name</label>
                                                            <input type="text" value="Martin" />
                                                        </div>
                                                    </Col>
                                                    <Col lg={6}>
                                                        <div className="pronamedetail">
                                                            <label>First Name</label>
                                                            <input type="text" value="Martin" />
                                                        </div>
                                                    </Col>

                                                </Row>
                                                <div className="savebtn">
                                                    <a href="#" className='btnblue'>Changes</a>
                                                    <a href="#" className='deactive'>Deactivate Account</a>
                                                </div>
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="second">
                                            <div className="prodetail">


                                                <ul className='addaddress'>
                                                    <li>
                                                        <div className="plusbtn">
                                                            <a href="#">+</a>
                                                            <span>Add New Address</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="addedaddress">
                                                            <span>Home</span>
                                                            <h5>Martin Willem</h5>
                                                            <p>A-432, Apple Square, 24th East Street,
                                                                New York, 960012</p>
                                                        </div>
                                                    </li>
                                                    {/* <li><Address people={people} /></li> */}
                                                </ul>
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="third">
                                            <div className="pswrddash">
                                                <label>Current Password</label>
                                                <input type="text" />
                                            </div>
                                            <div className="pswrddash">
                                                <label>New Password</label>
                                                <input type="text" />
                                            </div>
                                            <div className="pswrddash">
                                                <label>Confirm New Password</label>
                                                <input type="text" />
                                            </div>
                                            <div className="savebtn">
                                                <a href="#" className='btnblue'>Changes</a>
                                            </div>
                                        </Tab.Pane>
                                    </Tab.Content>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default EditProfile