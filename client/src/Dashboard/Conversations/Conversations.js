import React from "react";
import "./Conversations.css";
import Sidebar from "../Sidebar/Sidebar";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import { Container } from "react-bootstrap";
const Conversations = () => {
  return (
    <div className="dashboard">
      <Container>
        <Breadcrumb>
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>

          <Breadcrumb.Item active>Cart</Breadcrumb.Item>
        </Breadcrumb>
        <div className="maindash">
          <div className="sidebar">
            <Sidebar></Sidebar>
          </div>
          <div className="dashdetail">
            <h2>Purchase History</h2>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Conversations;
