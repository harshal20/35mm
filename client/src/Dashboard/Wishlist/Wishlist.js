import React from "react";
import "./Wishlist.css";
import Sidebar from "../Sidebar/Sidebar";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import proimg from "../../assets/images/product/purchasehistory/productimg1.png";
import proimg2 from "../../assets/images/product/purchasehistory/productimg2.png";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
const Wishlist = () => {
  return (
    <div className="dashboard">
      <Container>
        <Breadcrumb>
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>

          <Breadcrumb.Item active>Cart</Breadcrumb.Item>
        </Breadcrumb>
        <div className="maindash">
          <div className="sidebar">
            <Sidebar></Sidebar>
          </div>
          <div className="dashdetail">
            <h2>Wishlist</h2>

            <table className="protable">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Price</th>
                  <th>Product Name</th>
                  <th>Stock Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div className="proddetail">
                      <img src={proimg} alt="" />
                      <div className="proinner">
                        <h5>Long Strappy Dress</h5>
                        <span>
                          <svg
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M7.0001 10.2736L2.98559 12.462L3.88207 8.06746L0.503906 5.02543L5.07282 4.49774L7.0001 0.429138L8.92739 4.49774L13.4963 5.02543L10.1181 8.06746L11.0146 12.462L7.0001 10.2736Z"
                              fill="#FF7D51"
                            />
                          </svg>
                          5.0
                        </span>
                        <p>Color: Black-White</p>
                        <p>Size: S</p>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span className="pricedol">$130.00</span>
                  </td>
                  <td>
                    {" "}
                    <span className="pricedate">Aug 24, 2020</span>
                  </td>
                  <td>
                    <div className="stockstatus mb-0">
                      <p>3 in Stock</p>
                    </div>
                  </td>
                  <td>
                    <div className="btnaddcart">
                      <a href="#">Add to cart</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div className="proddetail">
                      <img src={proimg2} alt="" />
                      <div className="proinner">
                        <h5>Long Strappy Dress</h5>
                        <span>
                          <svg
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M7.0001 10.2736L2.98559 12.462L3.88207 8.06746L0.503906 5.02543L5.07282 4.49774L7.0001 0.429138L8.92739 4.49774L13.4963 5.02543L10.1181 8.06746L11.0146 12.462L7.0001 10.2736Z"
                              fill="#FF7D51"
                            />
                          </svg>
                          5.0
                        </span>
                        <p>Color: Black-White</p>
                        <p>Size: S</p>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span className="pricedol">$130.00</span>
                  </td>
                  <td>
                    {" "}
                    <span className="pricedate">Aug 24, 2020</span>
                  </td>
                  <td>
                    <div className="stockstatus mb-0">
                      <p>Out of Stock</p>
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    <div className="proddetail">
                      <img src={proimg2} alt="" />
                      <div className="proinner">
                        <h5>Long Strappy Dress</h5>
                        <span>
                          <svg
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M7.0001 10.2736L2.98559 12.462L3.88207 8.06746L0.503906 5.02543L5.07282 4.49774L7.0001 0.429138L8.92739 4.49774L13.4963 5.02543L10.1181 8.06746L11.0146 12.462L7.0001 10.2736Z"
                              fill="#FF7D51"
                            />
                          </svg>
                          5.0
                        </span>
                        <p>Color: Black-White</p>
                        <p>Size: S</p>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span className="pricedol">$130.00</span>
                  </td>
                  <td>
                    {" "}
                    <span className="pricedate">Aug 24, 2020</span>
                  </td>
                  <td>
                    <div className="stockstatus mb-0">
                      <p>5 in Stock</p>
                    </div>
                  </td>
                  <td>
                    <div className="btnaddcart">
                      <a href="#">Add to cart</a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Wishlist;
