import React from "react";
import "./dashboard.css";
import Sidebar from "../Sidebar/Sidebar";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import cartimg from "../../assets/img/cart.png";
import cartimg2 from "../../assets/img/cart2.png";
import cartimg3 from "../../assets/img/cart3.png";
import pro1 from "../../assets/img/cartimg.png";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

const Dashboard = () => {
  return (
    <div className="dashboard">
      <Container>
        <Breadcrumb>
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
          <Breadcrumb.Item active>Cart</Breadcrumb.Item>
        </Breadcrumb>
        <div className="maindash">
          <div className="sidebar">
            <Sidebar></Sidebar>
          </div>
          <div className="dashdetail">
            <ul className="cartcount">
              <li>
                <div className="cartcountsub">
                  <img src={cartimg} alt="" />
                  <span>02</span>
                  <p>Your Cart</p>
                </div>
              </li>
              <li>
                <div className="cartcountsub">
                  <img src={cartimg2} alt="" />
                  <span>05</span>
                  <p>Your Wishlist</p>
                </div>
              </li>
              <li>
                <div className="cartcountsub">
                  <img src={cartimg3} alt="" />
                  <span>08</span>
                  <p>Your Orders</p>
                </div>
              </li>
            </ul>
            <div className="cartprodetail">
              <div className="sendrequest">
                <span>
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 20 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M4.93628 3.80556C8.07878 1.23036 12.7229 1.40956 15.6566 4.34316C18.7807 7.46716 18.7807 12.5328 15.6566 15.6567C12.5325 18.7807 7.46676 18.7807 4.34266 15.6567C2.39372 13.7105 1.5839 10.8982 2.19939 8.21356L2.26099 7.96316L3.80824 8.37116C3.01638 11.3807 4.50416 14.5236 7.33383 15.8189C10.1635 17.1142 13.5148 16.1864 15.2754 13.6203C17.0359 11.0542 16.6954 7.59366 14.4685 5.41989C12.2415 3.24611 8.77368 2.98923 6.25073 4.81116L6.07392 4.94396L6.88755 5.75756L3.21062 6.60556L4.05865 2.92876L4.93628 3.80556ZM10.7997 5.19996V6.79996H12.7997V8.39996H8.3996C8.1924 8.39958 8.01921 8.55747 8.00048 8.76381C7.98176 8.97016 8.12371 9.15664 8.32759 9.19355L8.3996 9.19995H11.5997C12.7043 9.19995 13.5998 10.0954 13.5998 11.2C13.5998 12.3045 12.7043 13.2 11.5997 13.2H10.7997V14.7999H9.19962V13.2H7.19956V11.6H11.5997C11.8069 11.6003 11.9801 11.4424 11.9988 11.2361C12.0175 11.0298 11.8756 10.8433 11.6717 10.8064L11.5997 10.8H8.3996C7.29499 10.8 6.39953 9.90452 6.39953 8.79996C6.39953 7.69539 7.29499 6.79996 8.3996 6.79996L9.19962 6.79996V5.19996H10.7997Z"
                      fill="#1190CB"
                    />
                  </svg>
                  Sent Refund Request
                </span>
                <Link>
                  See All{" "}
                  <svg
                    width="8"
                    height="12"
                    viewBox="0 0 8 12"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M4.97656 5.99999L0.851562 1.87499L2.0299 0.696655L7.33323 5.99999L2.0299 11.3033L0.851562 10.125L4.97656 5.99999Z"
                      fill="#1190CB"
                    />
                  </svg>
                </Link>
              </div>
              <ul className="prodetail">
                <li>
                  <div className="proname">
                    <div className="proimgname">
                      <img src={pro1} alt="" />
                      <span>Long Strappy Dress</span>
                    </div>
                    <span>OD12366325220</span>
                    <span>23 Aug, 2020</span>
                    <p>$130.00</p>
                  </div>
                </li>
                <li>
                  <div className="proname">
                    <div className="proimgname">
                      <img src={pro1} alt="" />
                      <span>35mm Unique Bag - Yellow</span>
                    </div>
                    <span>OD12366325220</span>
                    <span>23 Aug, 2020</span>
                    <p>$130.00</p>
                  </div>
                </li>
                <li>
                  <div className="proname">
                    <div className="proimgname">
                      <img src={pro1} alt="" />
                      <span>Long Strappy Dress</span>
                    </div>
                    <span>OD12366325220</span>
                    <span>23 Aug, 2020</span>
                    <p>$130.00</p>
                  </div>
                </li>
                <li>
                  <div className="proname">
                    <div className="proimgname">
                      <img src={pro1} alt="" />
                      <span>Long Strappy Dress</span>
                    </div>
                    <span>OD12366325220</span>
                    <span>23 Aug, 2020</span>
                    <p>$130.00</p>
                  </div>
                </li>
                <li>
                  <div className="proname">
                    <div className="proimgname">
                      <img src={pro1} alt="" />
                      <span>Long Strappy Dress</span>
                    </div>
                    <span>OD12366325220</span>
                    <span>23 Aug, 2020</span>
                    <p>$130.00</p>
                  </div>
                </li>
                <li>
                  <div className="proname">
                    <div className="proimgname">
                      <img src={pro1} alt="" />
                      <span>Long Strappy Dress</span>
                    </div>
                    <span>OD12366325220</span>
                    <span>23 Aug, 2020</span>
                    <p>$130.00</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Dashboard;
